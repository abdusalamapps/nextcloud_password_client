// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/server_config_view_model.dart';
import 'server_config_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();

  test("fromMap", () {
    ServerConfigViewModel serverConfigViewModel =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    serverConfigViewModel.serverThemeBackground = '#000000';
    serverConfigViewModel.serverThemeLogo = 'none';
    serverConfigViewModel.serverThemeLabel = 'Testcloud';
    serverConfigViewModel.serverThemeAppIcon = 'someIcon';
    serverConfigViewModel.serverThemeFolderIcon = 'otherIcon';
    serverConfigViewModel.serverHandbookUrl = 'guide.unit.os';
    serverConfigViewModel.serverPerformance = 1;
    serverConfigViewModel.serverThemeColorPrimary = '#745bca';
    serverConfigViewModel.serverThemeColorText = '#ffffff';
    serverConfigViewModel.serverThemeColorBackground = '#ffffff';
    serverConfigViewModel.userSessionLifetime = 1600;
    serverConfigViewModel.serverVersion = '20220217';
    serverConfigViewModel.serverAppVersion = '2022.02.12';
    serverConfigViewModel.serverBaseUrl = 'test.unit.os';
    serverConfigViewModel.serverBaseUrlWebdav = 'webdav.code.os';
    serverConfigViewModel.serverSharingEnabled = true;
    serverConfigViewModel.serverSharingResharing = false;
    serverConfigViewModel.serverSharingAutocomplete = false;
    serverConfigViewModel.serverSharingTypes = ['user'];
    serverConfigViewModel.userPasswordGeneratorStrength = 1;
    serverConfigViewModel.userPasswordGeneratorNumbers = false;
    serverConfigViewModel.userPasswordGeneratorSpecial = true;
    serverConfigViewModel.usePasswordSecurityDuplicates = true;
    serverConfigViewModel.usePasswordSecurityAge = 2;
    serverConfigViewModel.userPasswordSecurityHash = 60;
    serverConfigViewModel.useMailSecurity = false;
    serverConfigViewModel.userMailShares = false;
    serverConfigViewModel.userNotificationSecurity = true;
    serverConfigViewModel.userNotificationShares = true;
    serverConfigViewModel.userNotificationErrors = true;
    serverConfigViewModel.userNotificationAdmin = true;
    serverConfigViewModel.userEncryptionSse = 1;
    serverConfigViewModel.userEncryptionCse = 1;
    serverConfigViewModel.userSharingEditable = false;
    serverConfigViewModel.userSharingResharing = false;
    ServerConfigViewModel serverConfigView = ServerConfigViewModel.fromMap(
        dataAccessLayer.samples['serverConfig'],
        dataAccessLayer: dataAccessLayer);
    expect(serverConfigViewModel, serverConfigView);
    serverConfigViewModel.serverSharingAutocomplete = true;
    serverConfigViewModel.userPasswordGeneratorStrength = 1;
    serverConfigViewModel.userPasswordGeneratorNumbers = false;
    serverConfigViewModel.userPasswordGeneratorSpecial = false;
    serverConfigViewModel.usePasswordSecurityDuplicates = true;
    serverConfigViewModel.serverThemeColorBackground = '#ffffff';
    Map<String, dynamic> config = dataAccessLayer.samples['serverConfig'];
    config.remove('user.password.generator.strength');
    config.remove('user.password.generator.numbers');
    config.remove('user.password.generator.special');
    config.remove('user.password.security.duplicates');
    config.remove('server.sharing.autocomplete');
    config.remove('server.theme.color.background');
    serverConfigView = ServerConfigViewModel.fromMap(
        dataAccessLayer.samples['serverConfig'],
        dataAccessLayer: dataAccessLayer);
    expect(serverConfigViewModel, serverConfigView);
  });
  test('refresh data model', () {
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    serverConfigView.serverBaseUrl = 'test.test.org';
    expect(serverConfigView.props == serverConfigView.serverConfigModel.props,
        false);
    serverConfigView.refreshModel(
        fromModel: serverConfigView,
        toModel: serverConfigView.serverConfigModel);
    expect(serverConfigView.props, serverConfigView.serverConfigModel.props);
  });

  test('refresh view model', () {
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    expect(serverConfigView.props, serverConfigView.serverConfigModel.props);
    serverConfigView.serverSharingEnabled = false;
    expect(serverConfigView.props == serverConfigView.serverConfigModel.props,
        false);
    serverConfigView.refreshModel(
        fromModel: serverConfigView.serverConfigModel,
        toModel: serverConfigView);
    expect(serverConfigView.props, serverConfigView.serverConfigModel.props);
  });

  test('handle logout', () {
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    serverConfigView.serverVersion = '1.2.3';
    expect(serverConfigView.props == serverConfigView.serverConfigModel.props,
        false);
    serverConfigView.handleLogout();
    expect(serverConfigView.props, serverConfigView.serverConfigModel.props);
  });
  test('persist config', () {
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    serverConfigView.serverConfigModel.serverThemeAppIcon = 'test';
    expect(serverConfigView.props == serverConfigView.serverConfigModel.props,
        false);

    when(serverConfigView.persistConfig()).thenAnswer((_) {
      verify(() => dataAccessLayer
          .persistServerConfig(serverConfigView.serverConfigModel)).called(1);
      expect(serverConfigView.props, serverConfigView.serverConfigModel.props);
    });
  });
  test('load config', () {
    clearInteractions(dataAccessLayer);
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    when(serverConfigView.loadServerConfig()).thenAnswer((_) {
      verify(() => dataAccessLayer.loadConfig()).called(1);
    });
  });

  test('initialize', () {
    clearInteractions(dataAccessLayer);
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    when(serverConfigView.initialize()).thenAnswer((_) async {
      verify(() => dataAccessLayer.loadConfig()).called(1);
    });
  });
  test("setServerConfiGModel", () {
    ServerConfigViewModel serverConfigView =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    ServerConfigModel serverConfigModel =
        ServerConfigViewModel(dataAccessLayer: dataAccessLayer);
    serverConfigModel.serverAppVersion = "1234567";
    expect(serverConfigView.props != serverConfigModel.props, true);
    serverConfigView.serverConfigModel = serverConfigModel;

    expect(serverConfigView.props, serverConfigView.serverConfigModel.props);
  });
}
