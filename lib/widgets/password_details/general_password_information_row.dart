// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/keyboard_shortcuts/add_new_tag.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/dialogues/password_editing_dialogue.dart';
import 'package:nextcloud_password_client/widgets/favorite_button.dart';
import 'package:nextcloud_password_client/widgets/formatted_date.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';
import 'package:nextcloud_password_client/widgets/tag_widget.dart';

Widget generalPasswordInformationRow(
    BuildContext context, PasswordViewModel password) {
  String updated = formattedDate(password.updated.toString());
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          password.favIconString.isNotEmpty
              ? Image.memory(
                  (base64.decode(password.favIconString)),
                )
              : const SizedBox.shrink(),
          const SizedBox(width: 10),
          Text(
            context
                .read<ConfigViewModel>()
                .keyChain
                .decrypt(password.cseKey, password.label),
          ),
          IconButton(
            tooltip: AppLocalizations.of(context)!.editPassword,
            onPressed: () async {
              await PasswordEditingDialogue.showPasswordEditDialogue(context);
            },
            icon: const Icon(Icons.edit),
          ),
          IconButton(
            onPressed: () async {
              PasswordListViewModel passwordListViewModel =
                  context.read<PasswordListViewModel>();
              passwordListViewModel.delete(context, password: password);
            },
            tooltip: AppLocalizations.of(context)!.deletePassword,
            icon: const Icon(Icons.delete),
          ),
        ],
      ),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          FavoriteIcon(password),
          Text(updated),
          const SizedBox(width: 5),
          if (password.tags.isNotEmpty)
            for (String tag in password.tags) tagWidget(password, context, tag),
          SizedBox(
            width: 200,
            child: AddNewTagShortcut(
              onAddNewTagDetected: () {
                password.addTag(
                    context, TextFieldController.tagTextController.text);
                TextFieldController.tagTextController.clear();
              },
              key: const Key('test'),
              child: getGeneralTextInput(
                TextFieldController.tagTextController,
                label: AppLocalizations.of(context)!.addTag,
              ),
            ),
          ),
        ],
      ),
    ],
  );
}
