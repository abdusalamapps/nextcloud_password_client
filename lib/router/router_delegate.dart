// Flutter imports:
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/page_config_constants.dart';
import 'package:nextcloud_password_client/enums/page_state.dart';
import 'package:nextcloud_password_client/enums/routing_path.dart';
import 'package:nextcloud_password_client/router/page_configuration.dart';
import 'package:nextcloud_password_client/screens/login_screen.dart';
import 'package:nextcloud_password_client/screens/missing_dependencies_screen.dart';
import 'package:nextcloud_password_client/screens/password_screen.dart';
import 'package:nextcloud_password_client/screens/settings_screen.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'page_action.dart';

class PasswordRouterDelegate extends RouterDelegate<PageConfiguration>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<PageConfiguration> {
  @override
  final GlobalKey<NavigatorState> navigatorKey;
  final List<MaterialPage> _pages = [];
  final ViewState viewState;

  PasswordRouterDelegate(this.viewState) : navigatorKey = GlobalKey() {
    viewState.addListener(() {
      notifyListeners();
    });
  }

  /// Getter for a list that cannot be changed
  List<MaterialPage> get pages => List.unmodifiable(_pages);

  /// Number of pages function
  int numPages() => _pages.length;

  @override
  PageConfiguration get currentConfiguration =>
      _pages.last.arguments as PageConfiguration;

  @override
  Future<void> setInitialRoutePath(configuration) {
    if (viewState.isLoggedIn) {
      return setNewRoutePath(passwordScreenPageConfig);
    } else {
      if (viewState.initError) {
        return setNewRoutePath(missingDependenciesPageConfig);
      } else {
        return setNewRoutePath(loginPageConfig);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      onPopPage: _onPopPage,
      pages: _buildPages(),
    );
  }

  bool _onPopPage(Route<dynamic> route, result) {
    final didPop = route.didPop(result);
    if (!didPop) {
      return false;
    }
    if (canPop()) {
      _pop();
      return true;
    } else {
      return false;
    }
  }

  void _removePage(MaterialPage page) {
    _pages.remove(page);
  }

  void _pop() {
    if (canPop()) {
      _removePage(_pages.last);
    }
  }

  bool canPop() {
    return _pages.length > 1;
  }

  @override
  Future<bool> popRoute() {
    if (canPop()) {
      _removePage(_pages.last);
      return Future.value(true);
    }
    return Future.value(false);
  }

  MaterialPage _createPage(Widget child, PageConfiguration pageConfig) {
    final LocalKey key = Key(pageConfig.key) as LocalKey;
    return MaterialPage(
        child: child, key: key, name: pageConfig.path, arguments: pageConfig);
  }

  void _addPageData(Widget child, PageConfiguration pageConfig) {
    _pages.add(
      _createPage(child, pageConfig),
    );
  }

  void _addPage(PageConfiguration? pageConfig) {
    final shouldAddPage = _pages.isEmpty ||
        (_pages.last.arguments as PageConfiguration).uiPage !=
            pageConfig!.uiPage;

    if (shouldAddPage) {
      switch (pageConfig!.uiPage) {
        case RoutingPath.login:
          _addPageData(const LoginScreen(), loginPageConfig);
          break;
        case RoutingPath.settings:
          _addPageData(const SettingsScreen(), settingsPageConfig);
          break;
        case RoutingPath.passwordScreen:
          _addPageData(const PasswordScreen(), passwordScreenPageConfig);
          break;
        case RoutingPath.missingDependenciesScreen:
          _addPageData(
              const MissingDependenciesScreen(), missingDependenciesPageConfig);
          break;
        default:
          break;
      }
    }
  }

  void replace(PageConfiguration? newRoute) {
    if (_pages.isNotEmpty) {
      _pages.removeLast();
    }
    _addPage(newRoute);
  }

  void setPath(List<MaterialPage> path) {
    _pages.clear();
    _pages.addAll(path);
  }

  void replaceAll(PageConfiguration? newRoute) {
    setNewRoutePath(newRoute);
  }

  void push(PageConfiguration? newRoute) {
    _addPage(newRoute);
  }

  void pushWidget(Widget child, PageConfiguration? newRoute) {
    _addPageData(child, newRoute!);
  }

  void addAll(List<PageConfiguration>? routes) {
    _pages.clear();
    if (routes != null) {
      for (var route in routes) {
        _addPage(route);
      }
    }
  }

  @override
  Future<void> setNewRoutePath(PageConfiguration? configuration) {
    final shouldAddPage = _pages.isEmpty ||
        (_pages.last.arguments as PageConfiguration).uiPage !=
            configuration!.uiPage;
    if (shouldAddPage) {
      _pages.clear();
      _addPage(configuration);
    }
    return SynchronousFuture(null);
  }

  void _setPageAction(PageAction action) {
    switch (action.page!.uiPage) {
      case RoutingPath.login:
        loginPageConfig.currentPageAction = action;
        break;
      case RoutingPath.settings:
        settingsPageConfig.currentPageAction = action;
        break;
      case RoutingPath.passwordScreen:
        passwordScreenPageConfig.currentPageAction = action;
        break;
      case RoutingPath.missingDependenciesScreen:
        missingDependenciesPageConfig.currentPageAction = action;
        break;
      default:
        break;
    }
  }

  List<Page> _buildPages() {
    if (!viewState.isLoggedIn) {
      if (viewState.initError) {
        replaceAll(missingDependenciesPageConfig);
      } else {
        replaceAll(loginPageConfig);
      }
    } else {
      switch (viewState.currentAction.state) {
        case PageState.none:
          break;
        case PageState.addPage:
          _setPageAction(viewState.currentAction);
          _addPage(viewState.currentAction.page);
          break;
        case PageState.pop:
          _pop();
          break;
        case PageState.replace:
          _setPageAction(viewState.currentAction);
          replace(viewState.currentAction.page);
          break;
        case PageState.replaceAll:
          _setPageAction(viewState.currentAction);
          replaceAll(viewState.currentAction.page);
          break;
        case PageState.addWidget:
          _setPageAction(viewState.currentAction);
          pushWidget(viewState.currentAction.widget as Widget,
              viewState.currentAction.page);
          break;
        case PageState.addAll:
          addAll(viewState.currentAction.pages);
          break;
      }
    }
    viewState.resetCurrentAction();
    return List.of(_pages);
  }
}
