// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'tag_list_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();

  List<TagViewModel> tagViewModels = [];
  tagViewModels.add(TagViewModel.fromMap({
    "id": "8c8f8f41-6941-4de2-895c-72c04b880d9e",
    "revision": "09373fe7-9e55-4b88-b506-3fb4d88d4b12",
    "label": "Social",
    "color": "#3498db",
    "edited": 1500151274,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "8f73f528-f017-444e-af7c-3fed4bac48b0",
    "revision": "59653ac7-74de-43b2-8007-5e8add20057f",
    "label": "Communication",
    "color": "#1abc9c",
    "edited": 1517949674,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "bec53e51-e556-4593-9445-2dd15908ba11",
    "revision": "1a1b87a7-6be7-47aa-b41f-f37453cacb5b",
    "label": "Shopping",
    "color": "#f39c12",
    "edited": 1499373674,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "1d0dae85-836d-4365-a0a8-2214e107fc5d",
    "revision": "1320d9df-1a61-46f7-9fae-8cb77cca937f",
    "label": "Money",
    "color": "#2ecc71",
    "edited": 1496090474,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "5f18853f-23f6-49d2-a340-a281dfc44b8f",
    "revision": "4c5b6d17-d1d3-46ee-8462-43c5aec7b988",
    "label": "Community",
    "color": "#e74c3c",
    "edited": 1499978474,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "070c24fc-be8d-4587-8093-da04664e3a11",
    "revision": "7321d207-f47a-4fca-b24c-6f400c20ad35",
    "label": "Software",
    "color": "#621980",
    "edited": 1552978370,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "3fe254fa-f60d-48a0-bc53-5523146dac1f",
    "revision": "df06ed08-d110-4157-96f1-856a65c50885",
    "label": "Messenger",
    "color": "#cddc39",
    "edited": 1507408874,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "918ae52e-573d-4e65-a1a0-a3b2fa6853f0",
    "revision": "482aa5ed-3292-4399-b5fd-d6db9309be02",
    "label": "Gaming",
    "color": "#e74856",
    "edited": 1507927274,
    "favorite": true
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "685879c2-da8a-4511-83a2-df9e51cfcfb9",
    "revision": "ee8c1085-6eb2-4d95-8c40-1d2095731281",
    "label": "Hardware",
    "color": "#95a5a6",
    "edited": 1517776874,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "85a1398b-89c6-4ca2-9816-135f7b364bbc",
    "revision": "597bc68e-11ff-49dc-9826-6e2b82a4e78d",
    "label": "Networking",
    "color": "#4eb9c6",
    "edited": 1556633322,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "3f93ebe7-2b55-4d5d-968c-142bb8bdaee7",
    "revision": "5601f941-2541-4776-a455-bc20cfedd8f1",
    "label": "Developer",
    "color": "#8bc34a",
    "edited": 1503261674,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "88ac6767-dec1-47a2-a95d-6a0de7ab0398",
    "revision": "c9220390-2e8b-4b5b-9b9e-37f2a72a828a",
    "label": "Crowdfunding",
    "color": "#ffc83b",
    "edited": 1552363893,
    "favorite": false
  }));
  tagViewModels.add(TagViewModel.fromMap({
    "id": "b71c274e-7076-4610-b261-6de6b32d426e",
    "revision": "9e52e5ab-6bfb-4817-9f1b-ff7ebf53a51d",
    "label": "Video",
    "color": "#37d7ab",
    "edited": 1490128874,
    "favorite": false
  }));
  TagViewModel tag = TagViewModel();
  tag.id = 'a9f90374-1599-46be-9eee-4ef2c2354be2';
  tag.revision = 'ce8e5130-a0be-41f6-a308-dba4457b5f6c';
  tag.label = 'Streaming';
  tag.color = '#5f5fc1';
  tag.edited = 1550675346;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = '21963f8d-3794-4fd7-8020-059fea33bf30';
  tag.revision = '57e96a3b-73c8-4ca9-b0b5-0b8171f52aeb';
  tag.label = 'Music';
  tag.color = '#03a9f4';
  tag.edited = 1490301674;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = '9ea06282-ae18-44fd-84a3-a1e9813712f6';
  tag.revision = 'e43fc638-1a42-4476-b979-a29d6384cf9e';
  tag.label = 'News';
  tag.color = '#0073d4';
  tag.edited = 1556633332;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = 'cff4271d-243a-4506-97e1-9a1ff23be8d8';
  tag.revision = 'e76123e4-8b9d-4816-b2a6-25c87a2f5046';
  tag.label = 'Support';
  tag.color = '#9E9E9E';
  tag.edited = 1503434474;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = 'e0187716-362e-4ed5-a0cf-bd390804a8d6';
  tag.revision = '0a1caa4f-980a-4b0c-b6c6-fd0d0cde8691';
  tag.label = 'Blogging';
  tag.color = '#FFEB3B';
  tag.edited = 1516567274;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = 'd244b461-c48f-43b1-b4c4-6a4093f57ccf';
  tag.revision = 'd3c74782-d042-4f80-899e-88f078c20242';
  tag.label = 'Cloud Services';
  tag.color = '#2196f3';
  tag.edited = 1520164982;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = 'fae0bb8b-1610-45da-a393-64ded6771525';
  tag.revision = '04e0f27a-1ed7-4b7c-b441-e745788fbc74';
  tag.label = 'IT';
  tag.color = '#1e61e9';
  tag.edited = 1552341736;
  tag.favorite = true;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = '5a854387-974c-4ecd-a6f6-3a79318edec7';
  tag.revision = '524f7e1e-7d26-4dae-a3c7-a97623279e60';
  tag.label = 'Photography';
  tag.color = '#4CAF50';
  tag.edited = 1556521324;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  tag = TagViewModel();
  tag.id = '1cdf735a-bc86-4574-b04a-0cd5d9db850c';
  tag.revision = 'd98ee842-5ce5-4307-ab56-36687dcdaf4d';
  tag.label = 'Travel';
  tag.color = '#4CAF50';
  tag.edited = 1556483691;
  tag.favorite = false;
  tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
  tagViewModels.add(tag);

  test('loadModel', () {
    List<TagModel> tagModels = [];
    for (TagViewModel tagView in tagViewModels) {
      tagModels.add(tagView.tagModel);
    }
    expect(listEquals(tagModels, dataAccessLayer.loadTags()), true);
  });
  test('initialize', () {
    clearInteractions(dataAccessLayer);
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    when(tagListViewModel.initialize()).thenAnswer((_) async {
      expect(tagViewModels == tagListViewModel.tagViewModels, true);
      verify(() => dataAccessLayer.loadTags()).called(1);
    });
  });

  test('persistModel', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    when(tagListViewModel.persistModel()).thenAnswer((_) async {
      verify(() => dataAccessLayer
          .persistTags([tagListViewModel.tagViewModels[1].tagModel])).called(1);
    });
  });
  test('setTagViewModels', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    expect(listEquals(tagViewModels, tagListViewModel.tagViewModels), false);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    expect(listEquals(tagViewModels, tagListViewModel.tagViewModels), true);
  });
  /*test('getTagByLabel', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    expect(tagViewModels[14], tagListViewModel.getTagByLabel('Music'));
  });*/
  test('getTagById', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    expect(tagViewModels[14],
        tagListViewModel.getTagById('21963f8d-3794-4fd7-8020-059fea33bf30'));
  });
  test('handleLOgout', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    expect(listEquals(tagViewModels, tagListViewModel.tagViewModels), true);
    tagListViewModel.handleLogout();
    expect(listEquals(tagViewModels, tagListViewModel.tagViewModels), false);
  });
  test('getFavoriteTags', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    List<TagViewModel> favTags = [];
    for (TagViewModel tagView in tagViewModels) {
      if (tagView.favorite) {
        favTags.add(tagView);
      }
    }
    expect(listEquals(favTags, tagListViewModel.getFavoriteTags()), true);
  });

  test('addTag', () {
    TagViewModel newTag = TagViewModel();
    newTag.label = 'TestTag';
    newTag.id = '1234566789';
    newTag.client = 'testClient';

    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    tagListViewModel.addTag(newTag);
    expect(newTag, tagListViewModel.getTagById('1234566789'));
  });
  test('deleteTag', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    TagViewModel delTag = TagViewModel.fromMap({
      "id": "3fe254fa-f60d-48a0-bc53-5523146dac1f",
      "revision": "df06ed08-d110-4157-96f1-856a65c50885",
      "label": "Messenger",
      "color": "#cddc39",
      "edited": 1507408874,
      "favorite": false
    });

    expect(listEquals(tagViewModels, tagListViewModel.tagViewModels), true);
    tagListViewModel.deleteTag(delTag);
    expect(listEquals(tagViewModels, tagListViewModel.tagViewModels), false);
    expect(TagViewModel(), tagListViewModel.getTagById(delTag.id));
  });
  test('refreshDataModel', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    List<TagModel> localModels = [];
    List<TagModel> listModels = [];

    for (TagViewModel tagView in tagViewModels) {
      TagViewModel newModel = TagViewModel.fromModel(tagView);
      newModel.hidden = newModel.tagModel.hidden = true;
      newModel.client = newModel.tagModel.client = 'test';
      localModels.add(newModel.tagModel);
    }

    for (TagViewModel tagView in tagListViewModel.tagViewModels) {
      tagView.client = 'test';
      tagView.hidden = true;
      listModels.add(tagView.tagModel);
    }

    localModels.sort((a, b) => a.id.compareTo(b.id));
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), false);
    tagListViewModel.refreshDataModels();
    listModels.clear();

    for (TagViewModel tagView in tagListViewModel.tagViewModels) {
      listModels.add(tagView.tagModel);
    }
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), true);
  });
  test('refreshViewModel', () {
    TagListViewModel tagListViewModel =
        TagListViewModel(dataAccessLayer: dataAccessLayer);
    tagListViewModel.tagViewModels = List<TagViewModel>.from(tagViewModels);
    List<TagViewModel> localModels = [];
    List<TagViewModel> listModels = [];

    for (TagViewModel tagView in tagViewModels) {
      TagViewModel newModel = TagViewModel.fromModel(tagView);
      newModel.favorite = newModel.tagModel.favorite = true;
      newModel.cseType = newModel.tagModel.cseType = 'test';
      localModels.add(newModel);
    }
    for (TagViewModel tagView in tagListViewModel.tagViewModels) {
      tagView.tagModel.favorite = true;
      tagView.tagModel.cseType = 'test';
      listModels.add(tagView);
    }

    localModels.sort((a, b) => a.id.compareTo(b.id));
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), false);

    tagListViewModel.refreshViewModels();
    listModels.clear();

    for (TagViewModel folderViewModel in tagListViewModel.tagViewModels) {
      listModels.add(folderViewModel);
    }
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), true);
  });
}
