// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'folder_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class FolderModelAdapter extends TypeAdapter<FolderModel> {
  @override
  final int typeId = 3;

  @override
  FolderModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return FolderModel()
      ..parent = fields[13] as String
      ..revisions = (fields[14] as List).cast<FolderModel>()
      ..id = fields[0] as String
      ..label = fields[1] as String
      ..revision = fields[2] as String
      ..cseType = fields[3] as String
      ..cseKey = fields[4] as String
      ..sseType = fields[5] as String
      ..client = fields[6] as String
      ..hidden = fields[7] as bool
      ..trashed = fields[8] as bool
      ..favorite = fields[9] as bool
      ..created = fields[10] as int
      ..updated = fields[11] as int
      ..edited = fields[12] as int;
  }

  @override
  void write(BinaryWriter writer, FolderModel obj) {
    writer
      ..writeByte(15)
      ..writeByte(13)
      ..write(obj.parent)
      ..writeByte(14)
      ..write(obj.revisions)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.label)
      ..writeByte(2)
      ..write(obj.revision)
      ..writeByte(3)
      ..write(obj.cseType)
      ..writeByte(4)
      ..write(obj.cseKey)
      ..writeByte(5)
      ..write(obj.sseType)
      ..writeByte(6)
      ..write(obj.client)
      ..writeByte(7)
      ..write(obj.hidden)
      ..writeByte(8)
      ..write(obj.trashed)
      ..writeByte(9)
      ..write(obj.favorite)
      ..writeByte(10)
      ..write(obj.created)
      ..writeByte(11)
      ..write(obj.updated)
      ..writeByte(12)
      ..write(obj.edited);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FolderModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
