// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/models/server_config_model.dart';
import 'package:nextcloud_password_client/themes/nextcloud_theme.dart';
import 'package:nextcloud_password_client/utils/theme_utils.dart';

void main() {
  test('getting theme by id', () {
    ServerConfigModel serverConfig = ServerConfigModel();
    expect(ThemeUtils.getTheme(Themes.dark, serverConfigModel: serverConfig),
        ThemeData.dark());
    expect(ThemeUtils.getTheme(Themes.light, serverConfigModel: serverConfig),
        ThemeData.light());
    expect(ThemeUtils.getTheme(Themes.custom, serverConfigModel: serverConfig),
        CustomThemes.customTheme);
    expect(
        ThemeUtils.getTheme(Themes.nextcloud, serverConfigModel: serverConfig),
        ThemeUtils.getTheme(Themes.nextcloud, serverConfigModel: serverConfig));
    expect(
        ThemeUtils.getTheme(Themes.nextcloud, serverConfigModel: serverConfig),
        ThemeUtils.getTheme(Themes.nextcloud));
    expect(
        ThemeUtils.getTheme(Themes.nextcloud, serverConfigModel: serverConfig),
        ThemeUtils.getTheme(Themes.nextcloud, serverConfigModel: serverConfig));
    expect(ThemeUtils.getTheme(Themes.nextcloud),
        ThemeUtils.getTheme(Themes.nextcloud));
    expect(ThemeUtils.getTheme(Themes.system, serverConfigModel: serverConfig),
        ThemeUtils.getTheme(Themes.nextcloud, serverConfigModel: serverConfig));
  });
}
