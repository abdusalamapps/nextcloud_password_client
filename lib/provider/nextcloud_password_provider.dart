// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:provider/provider.dart';

class NextcloudPasswordProvider {
  static Future<Map<String, List<PasswordViewModel>>> retrievePasswords(
      BuildContext context) async {
    CredentialsViewModel credentialsViewModel =
        context.read<CredentialsViewModel>();
    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
    http.Response passwordSessionResponse = await HttpUtils().httpPost(
        apiGetPasswordList,
        body: jsonEncode({'details': apiGetPasswordDetail}));
    var body = json.decode(passwordSessionResponse.body);
    Map<String, List<PasswordViewModel>> models = {};
    for (Map<String, dynamic> password in body) {
      PasswordViewModel pvm = PasswordViewModel.fromMap(password);
      // if (pvm.url.isNotEmpty) {
      //   String favIconUrl = apiGetFavIcon.replaceAll(
      //       '{domain}', Uri.parse(keyChain.decrypt(pvm.cseKey, pvm.url)).host);
      //   http.Response favIconResponse = await HttpUtils().httpGetFavIcon(
      //       favIconUrl,
      //       credentialsViewModel.userName,
      //       credentialsViewModel.password);
      //   if (favIconResponse.statusCode == 200) {
      //     pvm.favIconString = base64.encode(favIconResponse.bodyBytes);
      //     pvm.passwordModel.favIconString = pvm.favIconString;
      //   }
      // }
      if (!models.containsKey(pvm.folder)) {
        models[pvm.folder] = [];
      }
      models[pvm.folder]!.add(pvm);
    }
    return models;
  }
}
