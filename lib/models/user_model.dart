// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

part 'user_model.g.dart';

@HiveType(typeId: 14)
class UserModel extends ChangeNotifier{
  @HiveField(0)
  String id = '';
  @HiveField(1)
  String displayName = '';
  UserModel();
}