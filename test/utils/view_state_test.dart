// Flutter imports:
import 'package:flutter/cupertino.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'view_state_test.mocks.dart';

@GenerateMocks([HttpUtils, DataAccessLayer, BuildContext])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  ViewState viewState = ViewState(dataAccessLayer: dataAccessLayer);

  test("initialize", () {
    clearInteractions(dataAccessLayer);
    when(viewState.initialize()).thenAnswer((_) async {
      verify(() => dataAccessLayer.loadViewState()).called(1);
    });
  });

  test('resetViewState', () {
    viewState.selectedPassword = 'testPassword';
    viewState.isURLReachable = true;
    viewState.masterPasswordInputAnswer = true;
    viewState.isLoading = true;
    viewState.isLoggedIn = true;
    viewState.masterPasswordInputError = true;
    viewState.selectedSettingsItem = SettingsItems.about;
    viewState.clientPasswordInputError = true;
    viewState.credentialInputError = true;
    viewState.selectedFolder = 'testFolder';
    viewState.ignoreMissingCredentials = false;
    viewState.relogin = false;
    viewState.obscurePassword = false;
    viewState.folderSplitWeights = [0.5, 0.5];
    viewState.passwordSplitWeights = [0.2, 0.8];
    expect(viewState == ViewState(), false);
    viewState.resetViewState();
    //expect(viewState, ViewState());
  });
}
