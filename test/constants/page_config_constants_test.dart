// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/page_config_constants.dart';
import 'package:nextcloud_password_client/constants/page_config_key_constants.dart';
import 'package:nextcloud_password_client/constants/routing_paths_constants.dart';
import 'package:nextcloud_password_client/enums/routing_path.dart';
import 'package:nextcloud_password_client/router/page_configuration.dart';

void main() {
  test("loginPageConfig", () {
    PageConfiguration loginPage = PageConfiguration(
        key: pageKeyLogin, path: routingPathLogin, uiPage: RoutingPath.login);
    expect(loginPage, loginPageConfig);
  });
  test("settingsPageConfig", () {
    PageConfiguration settingsPage = PageConfiguration(
        key: pageKeySettings,
        path: routingPathSettings,
        uiPage: RoutingPath.settings);
    expect(settingsPage, settingsPageConfig);
  });
  test("passwordScreenPageConfig", () {
    PageConfiguration passwordScreenPage = PageConfiguration(
        key: pageKeyPasswordScreen,
        path: routingPathPasswordScreen,
        uiPage: RoutingPath.passwordScreen);
    expect(passwordScreenPage, passwordScreenPageConfig);
  });
}
