// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';

class FavoriteIcon extends StatefulWidget {
  final BaseModel baseModel;
  const FavoriteIcon(this.baseModel, {super.key});

  @override
  State<StatefulWidget> createState() => _FavoriteIconState();
}

class _FavoriteIconState extends State<FavoriteIcon> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: 20,
      splashRadius: 20,
      onPressed: () {
        widget.baseModel.favorite = !widget.baseModel.favorite;
        widget.baseModel
            .setFavorite(context, widget.baseModel.favorite)
            .then((returnText) => {
                  if (returnText.isNotEmpty)
                    {
                      showToast(
                          AppLocalizations.of(context)!
                                  .folderRemoteUpdateError +
                              AppLocalizations.of(context)!
                                  .localChangesOverwriteHint,
                          duration: const Duration(seconds: 5),
                          context: context)
                    }
                });
        setState(() {});
      },
      icon: widget.baseModel.favorite
          ? const Icon(Icons.star, color: Colors.yellow)
          : const Icon(Icons.star_border, color: Colors.yellow),
    );
  }
}
