// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/exceptions/missing_credentials_exception.dart';

void main() {
  test('Credentials Exception message', () {
    MissingCredentialsException excpetion =
        MissingCredentialsException('No credentials provided', 843);
    expect(excpetion.toString(),
        "Message : No credentials provided, Error code 843");

    expect(
        excpetion.toString() !=
            "Message : Wrong credentials entered with error code 111",
        true);
  });
}
