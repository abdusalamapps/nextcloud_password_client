// Project imports:
import 'package:nextcloud_password_client/models/tag_model.dart';

class TagViewModel extends TagModel {
  TagModel _tagModel = TagModel();
  List<TagViewModel> revisionViewModels = [];
  TagModel get tagModel => _tagModel;
  TagViewModel();

  @override
  List<dynamic> get props => super.props;
  TagViewModel.fromModel(TagModel model) {
    _tagModel = TagModel();
    refreshModel(fromModel: model, toModel: this);
    refreshModel(fromModel: model, toModel: _tagModel);
  }

  TagViewModel.fromMap(Map<String, dynamic> tag) {
    _tagModel = TagModel();
    _tagModel.id = id = tag['id'] ?? id;
    _tagModel.label = label = tag['label'] ?? label;
    _tagModel.revision = revision = tag['revision'] ?? revision;
    _tagModel.cseType = cseType = tag['cseType'] ?? cseType;
    _tagModel.cseKey = cseKey = tag['cseKey'] ?? cseKey;
    _tagModel.sseType = sseType = tag['sseType'] ?? sseType;
    _tagModel.client = client = tag['client'] ?? client;
    _tagModel.color = color = tag['color'] ?? color;
    _tagModel.hidden = hidden = tag['hidden'] ?? hidden;
    _tagModel.trashed = trashed = tag['trashed'] ?? trashed;
    _tagModel.favorite = favorite = tag['favorite'] ?? favorite;
    _tagModel.created = created = tag['created'] ?? created;
    _tagModel.updated = updated = tag['updated'] ?? updated;
    _tagModel.edited = edited = tag['edited'] ?? edited;
    if (tag['revisions'] != null) {
      revisionViewModels =
          getRevisionsFromAPI(List.castFrom(tag['revisions']));
      _tagModel.revisions = getRevisionModelFromViewModels();
    }
  }
  
  void refreshModel({required TagModel fromModel, required TagModel toModel}) {
    toModel.id = fromModel.id;
    toModel.label = fromModel.label;
    toModel.revision = fromModel.revision;
    toModel.cseType = fromModel.cseType;
    toModel.cseKey = fromModel.cseKey;
    toModel.sseType = fromModel.sseType;
    toModel.client = fromModel.client;
    toModel.color = fromModel.color;
    toModel.hidden = fromModel.hidden;
    toModel.trashed = fromModel.trashed;
    toModel.favorite = fromModel.favorite;
    toModel.created = fromModel.created;
    toModel.updated = fromModel.updated;
    toModel.edited = fromModel.edited;
    notifyListeners();
  }

  void refreshViewModel(TagModel tagModel) {
    refreshModel(fromModel: tagModel, toModel: this);
    revisionViewModels = getRevisionViewModelsFromModels(_tagModel);
    notifyListeners();
  }

  void refreshDataModel(TagViewModel tagViewModel) {
    refreshModel(fromModel: tagViewModel, toModel: _tagModel);
    _tagModel.revisions = getRevisionModelFromViewModels();
    notifyListeners();
  }
  
  List<TagViewModel> getRevisionsFromAPI(
      List<Map<String, dynamic>> revisions) {
    List<TagViewModel> models = [];
    for (Map<String, dynamic> revision in revisions) {
      models.add(TagViewModel.fromMap(revision));
    }
    return models;
  }

  List<TagModel> getRevisionModelFromViewModels() {
    List<TagModel> models = [];
    for (TagViewModel revision in revisionViewModels) {
      models.add(revision._tagModel);
    }
    return models;
  }

  List<TagViewModel> getRevisionViewModelsFromModels(
      TagModel tagModel) {
    List<TagViewModel> models = [];
    for (TagModel revision in tagModel.revisions) {
      models.add(TagViewModel.fromModel(revision));
    }
    return models;
  }
}


