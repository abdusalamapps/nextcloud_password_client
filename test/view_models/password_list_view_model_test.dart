// Flutter imports:
import 'package:flutter/foundation.dart';

// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'password_list_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  List<PasswordViewModel> passwordViewModels = [];
  Map<String, List<PasswordViewModel>> viewModelsMap = {};
  Map<String, List<PasswordViewModel>> listViewModelsMap = {};

  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "c20d541a-ee06-4269-a434-a12e378b5d58",
    "revision": "90e5be00-5c3c-4e71-8487-a137b499bba0",
    "label": "Facebook",
    "username": "zuckerberg",
    "password": "SozialisierungParteiischeres",
    "notes": "",
    "url": "https://www.facebook.com",
    "customFields": [],
    "folder": "53735697-0450-4bcc-961c-bb1877e99be1",
    "edited": 1506026474,
    "favorite": false,
    "tags": [
      "8c8f8f41-6941-4de2-895c-72c04b880d9e",
      "5f18853f-23f6-49d2-a340-a281dfc44b8f"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "0adf735d-dcbb-457f-b19a-3d850735ef7d",
    "revision": "c38a9b62-adae-4a38-a0e7-ffcebb34c17b",
    "label": "Google",
    "username": "page",
    "password": "DemolishedChoppingHarebrainedPinstripe",
    "notes": "",
    "url": "https://www.google.com",
    "customFields": [
      {
        "label": "E-Mail",
        "type": "email",
        "value": "page@gmail.com",
        "id": 0,
        "blank": false
      }
    ],
    "folder": "6bfd35b0-3998-49e8-b9f3-b8aebda9f0d3",
    "edited": 1550092766,
    "favorite": false,
    "tags": []
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "4a2f642a-b114-452e-8202-894242f41ba0",
    "revision": "8f292960-4be1-431e-b884-6607b7112346",
    "label": "Microsoft",
    "username": "gates",
    "password": "EdleresMarkierter",
    "notes": "",
    "url": "https://www.microsoft.com",
    "customFields": [],
    "folder": "7575ef5e-68ef-4bdb-bdb5-442accf73499",
    "edited": 1464640874,
    "favorite": false,
    "tags": ["070c24fc-be8d-4587-8093-da04664e3a11"]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "b92dfd4f-e9cd-479a-9dd8-a01b0cec2a76",
    "revision": "28f6ccc9-5443-40a9-9da2-bdad1ed60d55",
    "label": "Amazon",
    "username": "bezos",
    "password": "\"Mom€ntousB@llyhoo",
    "notes":
        "One of the largest online shopping websites in the world. The site is widely known for its wide selection of books, although the site has expanded to sell electronics, music, furniture, and apparel. Similar to eBay, users can also purchase and sell items using Amazon's online marketplace system. Amazon was founded in 1995 by Jeff Bezos and is based out of Seattle, Washington.",
    "url": "https://www.amazon.com",
    "customFields": [
      {
        "label": "Amaton Prime Pin",
        "type": "secret",
        "value": "1234",
        "id": 0,
        "blank": false
      },
      {
        "label": "E-Mail",
        "type": "email",
        "value": "mail@amazon.com",
        "id": 1,
        "blank": false
      }
    ],
    "folder": "fb8ab5f8-52ba-4c44-90a0-276a672e012d",
    "edited": 1555169699,
    "favorite": true,
    "share": {
      "created": 20220215050423,
      "editable": false,
      "expires": 20230123043123,
      "id": "fae0bb8b-1234-ertf-45df-64ded6771525",
      "owner": "you",
      "receiver": "me"
    },
    "tags": [
      "bec53e51-e556-4593-9445-2dd15908ba11",
      "b71c274e-7076-4610-b261-6de6b32d426e"
    ]
  }));

  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "7c33cc46-7548-4acf-9ead-3613ec11582d",
    "revision": "f0fd41e6-5cbd-4a6e-b52a-392150ea4487",
    "label": "skype.com - myskypename",
    "username": "myskypename",
    "password": "HineintretenderHelltönende",
    "notes": "",
    "url": "https://www.skype.com",
    "customFields": [],
    "folder": "53735697-0450-4bcc-961c-bb1877e99be1",
    "edited": 1478205674,
    "favorite": false,
    "tags": [
      "3fe254fa-f60d-48a0-bc53-5523146dac1f",
      "8c8f8f41-6941-4de2-895c-72c04b880d9e",
      "8f73f528-f017-444e-af7c-3fed4bac48b0"
    ]
  }));

  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "65696927-a9c8-4d67-8e65-1a1683343eb1",
    "revision": "29efc222-e236-4b22-9598-a4673d973fd1",
    "label": "apple.com - steve",
    "username": "steve",
    "password": "KnacklautenArbeitsgericht",
    "notes": "",
    "url": "https://www.apple.com",
    "customFields": [],
    "folder": "129fe6a7-84e1-4836-bb5f-8b0a0ce72928",
    "edited": 1519764074,
    "favorite": false,
    "tags": [
      "070c24fc-be8d-4587-8093-da04664e3a11",
      "685879c2-da8a-4511-83a2-df9e51cfcfb9"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "f9887869-3ce7-40df-8385-c659b1e4052c",
    "revision": "db137e5d-6fd4-41a6-ba61-3c1599ad641a",
    "label": "Whatsapp",
    "username": "+49123456",
    "password": "VorgezeichnetWeiträumigeres",
    "notes":
        "# WhatsApp\nMore than **1 billion people** in over **180 countries** use WhatsApp to stay in touch with friends and family, anytime and anywhere. WhatsApp is free and offers simple, secure, reliable messaging and calling, available on phones all over the world. \n\n\n## Our Mission\nWhatsApp started as an alternative to SMS. Our product now supports sending and receiving a variety of media: text, photos, videos, documents, and location, as well as voice calls. Our messages and calls are secured with end-to-end encryption, meaning that no third party including WhatsApp can read or listen to them. Behind every product decision is our desire to let people communicate anywhere in the world without barriers.\n\n## Our Team\nWhatsApp was founded by Jan Koum and Brian Acton who had previously spent 20 years combined at Yahoo. WhatsApp joined Facebook in 2014, but continues to operate as a separate app with a laser focus on building a messaging service that works fast and reliably anywhere in the world.\n\nFrom [WhatsApp](https://www.whatsapp.com/about/)",
    "url": "https://www.whatsapp.com",
    "customFields": [],
    "folder": "cd246f63-e36e-4bfe-b761-23ce17ccdd7a",
    "edited": 1486759274,
    "favorite": false,
    "tags": [
      "8c8f8f41-6941-4de2-895c-72c04b880d9e",
      "3fe254fa-f60d-48a0-bc53-5523146dac1f",
      "8f73f528-f017-444e-af7c-3fed4bac48b0"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "e843b5ec-c17b-400a-adb2-8b43da1d710a",
    "revision": "718e8f33-367b-487a-a974-21a178783715",
    "label": "Telegram",
    "username": "+49456789",
    "password": "AnwinkelndenVortritten",
    "notes": "",
    "url": "https://telegram.org/",
    "customFields": [],
    "folder": "cd246f63-e36e-4bfe-b761-23ce17ccdd7a",
    "edited": 1517172074,
    "favorite": true,
    "tags": [
      "8c8f8f41-6941-4de2-895c-72c04b880d9e",
      "3fe254fa-f60d-48a0-bc53-5523146dac1f",
      "8f73f528-f017-444e-af7c-3fed4bac48b0"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "699ba4c8-bb2f-401f-a593-ae86214c7c5c",
    "revision": "87f14633-747b-4e9b-95b4-0fc468830e95",
    "label": "Zalando",
    "username": "shoes",
    "password": "UnangebrachterGasfeuerung",
    "notes": "",
    "url": "https://www.zalando.de/",
    "customFields": [],
    "folder": "fb8ab5f8-52ba-4c44-90a0-276a672e012d",
    "edited": 1494103274,
    "favorite": false,
    "tags": ["bec53e51-e556-4593-9445-2dd15908ba11"]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "3e1f2ee5-0c65-49f3-8161-b05005262177",
    "revision": "d93ff28a-4869-4ec1-887f-e45f802974ff",
    "label": "Alternate.de",
    "username": "buypc",
    "password": "RatenzahlungsvereinbarungenGewinnanteils",
    "notes": "",
    "url": "https://www.alternate.de/",
    "customFields": [],
    "folder": "fb8ab5f8-52ba-4c44-90a0-276a672e012d",
    "edited": 1490820074,
    "favorite": false,
    "tags": [
      "bec53e51-e556-4593-9445-2dd15908ba11",
      "685879c2-da8a-4511-83a2-df9e51cfcfb9"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "56af42eb-1356-406f-8367-0335c9a0ec6d",
    "revision": "3473be02-db24-4132-8f8d-356ae7eb35ec",
    "label": "Conrad",
    "username": "someone",
    "password": "AugenfälligerenZeitplanens",
    "notes": "",
    "url": "https://www.conrad.de/",
    "customFields": [],
    "folder": "fb8ab5f8-52ba-4c44-90a0-276a672e012d",
    "edited": 1488746474,
    "favorite": false,
    "tags": [
      "bec53e51-e556-4593-9445-2dd15908ba11",
      "685879c2-da8a-4511-83a2-df9e51cfcfb9",
      "070c24fc-be8d-4587-8093-da04664e3a11"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "d8df515e-a13b-4c93-9a00-4fadd70ab2f0",
    "revision": "326fbd9e-9063-4ab5-8aad-76b77ff85067",
    "label": "Digitalo.de",
    "username": "buyer",
    "password": "VertrauensbildendemAusströmend",
    "notes": "",
    "url": "https://www.digitalo.de/",
    "customFields": [],
    "folder": "fb8ab5f8-52ba-4c44-90a0-276a672e012d",
    "edited": 1507581674,
    "favorite": false,
    "tags": [
      "bec53e51-e556-4593-9445-2dd15908ba11",
      "685879c2-da8a-4511-83a2-df9e51cfcfb9"
    ]
  }));
  passwordViewModels.add(PasswordViewModel.fromMap({
    "id": "5d24318f-4441-48ae-a074-6a9ba3927ed8",
    "revision": "7cc2a566-78ff-4d09-a67b-c1e6a0d868fb",
    "label": "YouTube",
    "username": "famous",
    "password": "DurchstreiftenEs",
    "notes": "",
    "url": "https://www.youtube.com/",
    "customFields": [],
    "folder": "2f07fbd5-e9ec-4c4a-9d1b-7590387bc084",
    "edited": 1481229674,
    "favorite": false,
    "tags": [
      "5f18853f-23f6-49d2-a340-a281dfc44b8f",
      "8c8f8f41-6941-4de2-895c-72c04b880d9e",
      "a9f90374-1599-46be-9eee-4ef2c2354be2",
      "b71c274e-7076-4610-b261-6de6b32d426e"
    ]
  }));
  PasswordViewModel newModel = PasswordViewModel();
  newModel.id = 'd9f2195a-a622-436d-893d-fb3c31e5de08';
  newModel.revision = '9c5157e3-4e87-4bd1-81a7-6c64e5d65f87';
  newModel.label = 'Twitch.tv';
  newModel.username = 'streamer';
  newModel.password = 'LeftTraveledMeritingWarlord';
  newModel.url = 'https://www.twitch.tv/';
  newModel.folder = '2f07fbd5-e9ec-4c4a-9d1b-7590387bc084';
  newModel.edited = 1555145953;
  newModel.favorite = false;
  newModel.tags.addAll([
    '918ae52e-573d-4e65-a1a0-a3b2fa6853f0',
    '5f18853f-23f6-49d2-a340-a281dfc44b8f',
    '8c8f8f41-6941-4de2-895c-72c04b880d9e',
    'a9f90374-1599-46be-9eee-4ef2c2354be2'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = 'cff313b2-4375-46f5-9265-bc4f6887b35c';
  newModel.revision = '226d163e-4919-47e7-9fd6-04dbdb410737';
  newModel.label = 'Twitter';
  newModel.username = 'dorsey';
  newModel.password = 'HergestelltemZurückbehaltener';
  newModel.url = 'https://twitter.com/';
  newModel.folder = '129fe6a7-84e1-4836-bb5f-8b0a0ce72928';
  newModel.edited = 1464813674;
  newModel.favorite = false;
  newModel.tags.addAll([
    '5f18853f-23f6-49d2-a340-a281dfc44b8f',
    '8f73f528-f017-444e-af7c-3fed4bac48b0'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '4cf677e6-e87c-4d24-a69d-09cae138ba4f';
  newModel.revision = '7e1d2eea-33be-44ef-9955-d682b555585b';
  newModel.label = 'Google Plus';
  newModel.username = 'brin';
  newModel.password = 'VerrostetAufpumpenden';
  newModel.url = 'https://plus.google.com/';
  newModel.folder = '53735697-0450-4bcc-961c-bb1877e99be1';
  newModel.edited = 1494016874;
  newModel.favorite = false;
  newModel.tags.addAll([
    'f18853f-23f6-49d2-a340-a281dfc44b8f',
    '8c8f8f41-6941-4de2-895c-72c04b880d9e'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '83da85f5-ede4-4342-9dbe-c45ab51cd8b3';
  newModel.revision = 'd5ee1dac-100b-4793-965b-2dd07b15d7eb';
  newModel.label = 'Teamviewer';
  newModel.username = 'desktop';
  newModel.password = 'DemontagefallHerausgewagter';
  newModel.url = 'https://www.teamviewer.com/de/';
  newModel.folder = 'b2ab16e6-994c-47ac-8324-7b6c0f6f4553';
  newModel.edited = 1492980074;
  newModel.favorite = false;
  newModel.tags.addAll(['3fe254fa-f60d-48a0-bc53-5523146dac1f']);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = 'b4772bdd-efb0-4c61-9cf2-782187be0424';
  newModel.revision = '34acd33e-24e6-451f-b011-f1fd4e94e901';
  newModel.label = 'Slack';
  newModel.username = 'chat';
  newModel.password = 'NeuphilologischeSäurehaltigerem';
  newModel.url = 'https://slack.com/intl/de-de';
  newModel.folder = '7575ef5e-68ef-4bdb-bdb5-442accf73499';
  newModel.edited = 1515530474;
  newModel.favorite = false;
  newModel.tags.addAll([
    '8f73f528-f017-444e-af7c-3fed4bac48b0',
    '3fe254fa-f60d-48a0-bc53-5523146dac1f',
    '8c8f8f41-6941-4de2-895c-72c04b880d9e'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '1713c167-573c-41de-9449-251435e0e65e';
  newModel.revision = '54e295a1-03a2-4992-9631-cb3f633229e8';
  newModel.label = 'Deutsche Bank';
  newModel.username = 'bank';
  newModel.password = 'InstrumentationenHineinführend';
  newModel.url = 'https://www.deutsche-bank.de/pfb/content/privatkunden.html';
  newModel.folder = '973faede-9bbf-4fb2-a625-a34acbbe6e8b';
  newModel.edited = 1488055274;
  newModel.favorite = false;
  newModel.tags.addAll(['1d0dae85-836d-4365-a0a8-2214e107fc5d']);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '888a11c5-35fb-445d-97ec-2d9646842eae';
  newModel.revision = '0d020cc9-72f2-4ba2-ad1f-82b1bc98b885';
  newModel.label = 'PayPal';
  newModel.username = 'pay';
  newModel.password = 'PreiserhöhungAbgebürstete';
  newModel.url = 'https://www.paypal.com/de/webapps/mpp/home';
  newModel.folder = '973faede-9bbf-4fb2-a625-a34acbbe6e8b';
  newModel.edited = 1460320874;
  newModel.favorite = false;
  newModel.tags.addAll(['1d0dae85-836d-4365-a0a8-2214e107fc5d']);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '0311cb52-95d9-4b27-bb6f-36a3fba07441';
  newModel.revision = 'db48e68a-21a1-486f-86c0-cc60810b1ae4';
  newModel.label = 'PayDirekt';
  newModel.username = 'geld';
  newModel.password = 'MorgenstundeAnklagst';
  newModel.url = 'https://www.paydirekt.de/account/#/login?registration';
  newModel.folder = '973faede-9bbf-4fb2-a625-a34acbbe6e8b';
  newModel.edited = 1513888874;
  newModel.favorite = false;
  newModel.tags.addAll(['1d0dae85-836d-4365-a0a8-2214e107fc5d']);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = 'f1026b6e-a5fb-41e7-803d-482dd4b766db';
  newModel.revision = '593f5a1a-8218-4f66-a268-48a3cbe6afe9';
  newModel.label = 'ebay.de - seller';
  newModel.username = 'seller';
  newModel.password = 'HerzigerHerzförmiger';
  newModel.url = 'https://www.ebay.de/';
  newModel.folder = 'fb8ab5f8-52ba-4c44-90a0-276a672e012d';
  newModel.edited = 1461703274;
  newModel.favorite = false;
  newModel.tags.addAll(['bec53e51-e556-4593-9445-2dd15908ba11']);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = 'd21ef287-2cff-43a7-979c-2ed7117b547d';
  newModel.revision = 'fc664c4c-6841-4105-92cc-52a2eb6cf0b2';
  newModel.label = 'Stackoverflow';
  newModel.username = 'ask';
  newModel.password = 'Incumb3ntTamab1e';
  newModel.url = 'https://stackoverflow.com/';
  newModel.folder = '77f4d132-7be3-4592-9b6b-0268380fd803';
  newModel.edited = 1555079037;
  newModel.favorite = false;
  newModel.tags.addAll([
    '5f18853f-23f6-49d2-a340-a281dfc44b8f',
    '070c24fc-be8d-4587-8093-da04664e3a11',
    '3f93ebe7-2b55-4d5d-968c-142bb8bdaee7'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '038cb1e8-f588-44bf-b40f-bc5c53b5978c';
  newModel.revision = 'c182d121-21f7-4447-8a42-433f57512a68';
  newModel.label = 'Amazon AWS';
  newModel.customFields = '[]';
  newModel.username = 'hosting';
  newModel.password = 'EndlessWiredAloofTrammels';
  newModel.url = 'https://aws.amazon.com/de/';
  newModel.folder = '42bfa1d6-ce03-4f95-a127-e41ebcc6080e';
  newModel.edited = 1544823755;
  newModel.favorite = false;
  newModel.tags.addAll([
    '070c24fc-be8d-4587-8093-da04664e3a11',
    '3f93ebe7-2b55-4d5d-968c-142bb8bdaee7',
    'd244b461-c48f-43b1-b4c4-6a4093f57ccf'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);

  newModel = PasswordViewModel();
  newModel.id = '30265437-4a28-4ece-a4ef-f07eceeb97c0';
  newModel.revision = '9d2cd4ea-8489-4aa4-8f65-2aa6130e1b09';
  newModel.label = 'Steam';
  newModel.username = 'gabe';
  newModel.password = 'FarmTransmittedUnbrokenShoehorn';
  newModel.notes =
      "# Steam\n**Steam** is a digital distribution platform developed by **Valve** Corporation, which offers digital rights management (DRM), multiplayer gaming, video streaming and social networking services. Steam provides the user with installation and automatic updating of games, and community features such as friends lists and groups, cloud saving, and in-game voice and chat functionality. The software provides a freely available application programming interface (API) called Steamworks, which developers can use to integrate many of Steam's functions into their products, including networking, matchmaking, in-game achievements, micro-transactions, and support for user-created content through Steam Workshop. Though initially developed for use on Microsoft Windows operating systems, versions for OS X and Linux were later released. Mobile apps with connected functionality with the main software were later released for iOS, Android, and Windows Phone devices in the 2010s.";
  newModel.url = 'http://store.steampowered.com/';
  newModel.folder = '593b0f5b-c257-4c15-b2fe-386253524832';
  newModel.edited = 1548555828;
  newModel.favorite = false;
  newModel.customFields = [
    {
      "label": "Username",
      "type": "text",
      "value": "XXXLolz",
      "id": 0,
      "blank": false
    },
    {
      "label": "E-Mail",
      "type": "email",
      "value": "loluser@passwords.app",
      "id": 1,
      "blank": false
    }
  ].toString();
  newModel.tags.addAll([
    '070c24fc-be8d-4587-8093-da04664e3a11',
    '5f18853f-23f6-49d2-a340-a281dfc44b8f',
    '8c8f8f41-6941-4de2-895c-72c04b880d9e',
    'bec53e51-e556-4593-9445-2dd15908ba11',
    '918ae52e-573d-4e65-a1a0-a3b2fa6853f0'
  ]);
  newModel.refreshDataModel(newModel);
  passwordViewModels.add(newModel);
  for (PasswordViewModel password in passwordViewModels) {
    if (!viewModelsMap.containsKey(password.folder)) {
      viewModelsMap[password.folder] = [];
    }
    viewModelsMap[password.folder]!.add(PasswordViewModel.fromModel(password));
  }

  for (PasswordModel passwordModel in dataAccessLayer.loadPasswords()) {
    newModel = PasswordViewModel.fromModel(passwordModel);
    if (passwordViewModels.contains(newModel)) {
      if (!listViewModelsMap.containsKey(newModel.folder)) {
        listViewModelsMap[newModel.folder] = [];
      }
      listViewModelsMap[newModel.folder]!.add(newModel);
    }
  }

  test('initialization', () {
    clearInteractions(dataAccessLayer);
    PasswordListViewModel passwordListViewModel =
        PasswordListViewModel(dataAccessLayer: dataAccessLayer);
    passwordListViewModel.passwordViewModels = listViewModelsMap;
    when(passwordListViewModel.initialize()).thenAnswer((_) async {
      expect(mapEquals(viewModelsMap, listViewModelsMap), true);
      verify(() => dataAccessLayer.loadPasswords()).called(1);
    });
  });

  test('persistModel', () {
    clearInteractions(dataAccessLayer);
    PasswordListViewModel passwordListViewModel =
        PasswordListViewModel(dataAccessLayer: dataAccessLayer);
    passwordListViewModel.passwordViewModels = listViewModelsMap;
    when(passwordListViewModel.persistModel()).thenAnswer((_) async {
      verify(() => dataAccessLayer.persistPasswords(passwordViewModels))
          .called(1);
    });
  });

  test('getPasswordById', () {
    PasswordListViewModel passwordListViewModel =
        PasswordListViewModel(dataAccessLayer: dataAccessLayer);
    passwordListViewModel.passwordViewModels = listViewModelsMap;
    expect(
        passwordListViewModel
            .getPasswordById('038cb1e8-f588-44bf-b40f-bc5c53b5978c'),
        passwordViewModels[23]);
  });

  test("refreshViewModel", () {
    PasswordListViewModel passwordListViewModel =
        PasswordListViewModel(dataAccessLayer: dataAccessLayer);
    passwordListViewModel.passwordViewModels =
        Map<String, List<PasswordViewModel>>.from(listViewModelsMap);
    List<PasswordViewModel> localModels = [];
    List<PasswordViewModel> listModels = [];

    for (List<PasswordViewModel> passwordViewList in listViewModelsMap.values) {
      for (PasswordViewModel passwordView in passwordViewList) {
        PasswordViewModel newModel = PasswordViewModel.fromModel(passwordView);
        newModel.notes = newModel.passwordModel.notes = 'here is a note';
        newModel.password =
            newModel.passwordModel.password = 'extremeSecurePassword';
        localModels.add(newModel);
      }
    }

    for (List<PasswordViewModel> passwordViewList
        in passwordListViewModel.passwordViewModels.values) {
      for (PasswordViewModel passwordView in passwordViewList) {
        passwordView.passwordModel.notes = 'here is a note';
        passwordView.passwordModel.password = 'extremeSecurePassword';
        listModels.add(passwordView);
      }
    }
    localModels.sort((a, b) => a.id.compareTo(b.id));
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), false);

    passwordListViewModel.refreshViewModels();
    listModels.clear();

    for (List<PasswordViewModel> passwordViewList
        in passwordListViewModel.passwordViewModels.values) {
      for (PasswordViewModel passwordView in passwordViewList) {
        listModels.add(passwordView);
      }
    }
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), true);
  });
  test("refreshDataModel", () {
    PasswordListViewModel passwordListViewModel =
        PasswordListViewModel(dataAccessLayer: dataAccessLayer);
    passwordListViewModel.passwordViewModels =
        Map<String, List<PasswordViewModel>>.from(listViewModelsMap);
    List<PasswordModel> localModels = [];
    List<PasswordModel> listModels = [];

    for (List<PasswordViewModel> passwordViewList in listViewModelsMap.values) {
      for (PasswordViewModel passwordView in passwordViewList) {
        PasswordViewModel newModel = PasswordViewModel.fromModel(passwordView);
        newModel.editable = newModel.passwordModel.editable = true;
        newModel.label = newModel.passwordModel.label = 'testLabel';
        localModels.add(newModel.passwordModel);
      }
    }

    for (List<PasswordViewModel> passwordViewList
        in passwordListViewModel.passwordViewModels.values) {
      for (PasswordViewModel passwordView in passwordViewList) {
        passwordView.editable = true;
        passwordView.label = 'testLabel';
        listModels.add(passwordView.passwordModel);
      }
    }

    localModels.sort((a, b) => a.id.compareTo(b.id));
    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), false);
    passwordListViewModel.refreshDataModels();
    listModels.clear();

    for (List<PasswordViewModel> passwordViewList
        in passwordListViewModel.passwordViewModels.values) {
      for (PasswordViewModel passwordView in passwordViewList) {
        listModels.add(passwordView.passwordModel);
      }
    }

    listModels.sort((a, b) => a.id.compareTo(b.id));
    expect(listEquals(localModels, listModels), true);
  });

  test('handleLogout', () {
    PasswordListViewModel passwordListViewModel =
        PasswordListViewModel(dataAccessLayer: dataAccessLayer);
    passwordListViewModel.passwordViewModels =
        Map<String, List<PasswordViewModel>>.from(listViewModelsMap);
    expect(
        mapEquals(listViewModelsMap, passwordListViewModel.passwordViewModels),
        true);
    passwordListViewModel.handleLogout();
    expect(passwordListViewModel.passwordViewModels.isEmpty, true);
  });
}
