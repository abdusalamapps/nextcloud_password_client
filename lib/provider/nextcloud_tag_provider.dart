// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class NextcloudTagProvider {
  static Future<List<TagViewModel>> retrieveTags() async {
    http.Response response =
        await HttpUtils().httpPost(apiGetTagList, body: jsonEncode({'details': apiGetTagDetail}));
   var body = json.decode(response.body);
    List<TagViewModel> models = [];
    for (Map<String, dynamic> tag in body) {
      TagViewModel tagViewModel = TagViewModel.fromMap(tag);
      models.add(tagViewModel);
    }
    return models;
  }

  static Future<bool> deleteTag(String tagBody) async{
    http.Response response  = await HttpUtils().httpDelete(apiPostTagDelete, body: tagBody);
    return response.statusCode == 200;
  }
}
