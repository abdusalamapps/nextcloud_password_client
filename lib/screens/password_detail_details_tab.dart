// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_details/password_general_details.dart';
import 'package:nextcloud_password_client/widgets/password_details/password_security_details.dart';
import 'package:nextcloud_password_client/widgets/password_details/password_statistics_details.dart';

Widget getPasswordDetailTab(BuildContext context, PasswordViewModel password) {
  ConfigViewModel configViewModel = context.read<ConfigViewModel>();
  TextEditingController passwordController =
      TextFieldController.passwordController;
  passwordController.text =
      configViewModel.keyChain.decrypt(password.cseKey, password.password);
  final ScrollController detailScrollController = ScrollController();
  final ScrollController detailScrollController2 = ScrollController();
  return SingleChildScrollView(
    controller: detailScrollController,
    scrollDirection: Axis.vertical,
    child: SingleChildScrollView(
      controller: detailScrollController2,
      scrollDirection: Axis.horizontal,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(child: generalPasswordDetails(context, password)),
          Container(child: passwordStatisticsDetails(context, password)),
          Container(child: passwordSecurityDetails(context, password)),
        ],
      ),
    ),
  );
}
