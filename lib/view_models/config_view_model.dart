// Package imports:

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/config_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';

class ConfigViewModel extends ConfigModel implements ViewModelInterface {
  
  ConfigModel _configModel = ConfigModel();
  DataAccessLayer dataAccessLayer;

  @override
  Future<void> initialize() async {
    loadModel();
    refreshViewModels();
  }

  ConfigViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  int geTemporaryID() {
    $temporaryID += 1;
    persistModel();
    return $temporaryID;
  }

  @override
  Themes get selectedTheme => $selectedTheme;

  @override
  bool get saveCredentials => $saveCredentials;

  @override
  bool get saveMasterPassword => $saveMasterPassword;

  @override
  bool get useLocalCopy => $useLocalCopy;

  ConfigModel get configModel => _configModel;

  set selectedTheme(Themes theme) {
    $selectedTheme = theme;
    notifyListeners();
  }

  set refreshRateInSeconds(int refreshRate) {
    $refreshRateInSeconds = refreshRate;
    notifyListeners();
  }

  set saveMasterPassword(saveMasterPassword) {
    $saveMasterPassword = saveMasterPassword;
    notifyListeners();
  }

  set useLocalCopy(useLocalCopy) {
    $useLocalCopy = useLocalCopy;
    notifyListeners();
  }

  set saveCredentials(saveCredentials) {
    $saveCredentials = saveCredentials;
    notifyListeners();
  }

  @override
  void refreshDataModels() {
    refreshModel(fromModel: this, toModel: _configModel);
  }

  @override
  void refreshViewModels() {
    refreshModel(fromModel: _configModel, toModel: this);
  }

  refreshModel({required ConfigModel fromModel, required ConfigModel toModel}) {
    toModel.serverURL = fromModel.serverURL;
    toModel.$useLocalCopy = fromModel.$useLocalCopy;
    toModel.masterPasswordRequired = fromModel.masterPasswordRequired;
    toModel.$saveCredentials = fromModel.saveCredentials;
    toModel.$saveMasterPassword = fromModel.$saveMasterPassword;
    toModel.loginSucceeded = fromModel.loginSucceeded;
    toModel.session = fromModel.session;
    toModel.keyChain = fromModel.keyChain;
    toModel.$selectedTheme = fromModel.$selectedTheme;
    toModel.$refreshRateInSeconds = fromModel.$refreshRateInSeconds;
    notifyListeners();
  }

  @override
  void persistModel([bool firstBool = false, bool secondBool = false]) async {
    refreshDataModels();
    dataAccessLayer.persistConfig(_configModel);
  }

  @override
  void loadModel() {
    _configModel = dataAccessLayer.loadConfig()!;
  }

  @override
  void handleLogout() {
    _configModel = ConfigModel();
    refreshViewModels();
  }
}
