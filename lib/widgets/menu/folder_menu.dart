// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/menu_constants.dart';
import 'package:nextcloud_password_client/widgets/menu/selection_handler.dart';

Widget getFolderMenu(BuildContext context) {
  return PopupMenuButton<String>(
    tooltip: AppLocalizations.of(context)!.folderMenu,
    onSelected: (value) => handleMenuItemSelection(context, value),
    itemBuilder: (context) => <PopupMenuEntry<String>>[
      PopupMenuItem<String>(
        value: createFolder,
        child: ListTile(
          leading: const Icon(Icons.add),
          title: Text(AppLocalizations.of(context)!.createFolder),
        ),
      ),
      PopupMenuItem<String>(
        value: editFolder,
        child: ListTile(
          leading: const Icon(Icons.edit),
          title: Text(AppLocalizations.of(context)!.editFolder),
        ),
      ),
      PopupMenuItem<String>(
        value: deleteFolder,
        child: ListTile(
          leading: const Icon(Icons.delete),
          title: Text(AppLocalizations.of(context)!.deleteFolder),
        ),
      ),
    ],
    child: Text(AppLocalizations.of(context)!.folder),
  );
}
