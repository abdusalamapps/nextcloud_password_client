// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/exceptions/decrypt_exception.dart';

void main() {
  test('Credentials Exception message', () {
    DecryptException exception = DecryptException('problems decrypting value');

    expect(exception.toString(), 'problems decrypting value');
    expect(exception.toString() != "some problems here", true);
  });
}
