// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'credentials_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();

  test('initialize', () {
    CredentialsViewModel credentialsViewModel =
        CredentialsViewModel(dataAccessLayer: dataAccessLayer);
    when(credentialsViewModel.initialize()).thenAnswer((_) async {
      verify(() => dataAccessLayer.loadConfig()).called(1);
    });
  });

  test('refresh data model', () {
    CredentialsViewModel credentialsViewModel =
        CredentialsViewModel(dataAccessLayer: dataAccessLayer);
    credentialsViewModel.userName = 'testUser';
    expect(
        credentialsViewModel.props ==
            credentialsViewModel.credentialsModel.props,
        false);
    credentialsViewModel.refreshDataModels();
    expect(credentialsViewModel.props,
        credentialsViewModel.credentialsModel.props);
  });

  test('refresh view model', () {
    CredentialsViewModel credentialsViewModel =
        CredentialsViewModel(dataAccessLayer: dataAccessLayer);
    expect(credentialsViewModel.clientPassword,
        credentialsViewModel.credentialsModel.clientPassword);
    credentialsViewModel.credentialsModel.clientPassword = 'antotherPassword';
    expect(
        credentialsViewModel.props ==
            credentialsViewModel.credentialsModel.props,
        false);
    credentialsViewModel.refreshViewModels();
    expect(credentialsViewModel.props,
        credentialsViewModel.credentialsModel.props);
  });

  test('persist credentials', () {
    CredentialsViewModel credentialsViewModel =
        CredentialsViewModel(dataAccessLayer: dataAccessLayer);
    credentialsViewModel.password = 'TestPassword';
    credentialsViewModel.masterPassword = 'TestMaster';
    expect(
        credentialsViewModel.props ==
            credentialsViewModel.credentialsModel.props,
        false);
    expect(
        credentialsViewModel.props ==
            credentialsViewModel.credentialsModel.props,
        false);
    when(credentialsViewModel.persistModel(true, false)).thenAnswer((_) {
      verify(() => dataAccessLayer
          .persistCredentials(credentialsViewModel.credentialsModel)).called(1);
    });
    expect(credentialsViewModel.password,
        credentialsViewModel.credentialsModel.password);
    expect(
        credentialsViewModel.masterPassword ==
            credentialsViewModel.credentialsModel.masterPassword,
        false);

    credentialsViewModel.password = 'AnotherPassword';
    credentialsViewModel.masterPassword = 'AnotherMaster';
    expect(
        credentialsViewModel.password ==
            credentialsViewModel.credentialsModel.password,
        false);
    expect(
        credentialsViewModel.masterPassword ==
            credentialsViewModel.credentialsModel.masterPassword,
        false);
    when(credentialsViewModel.persistModel(false, true)).thenAnswer((_) {
      verify(() => dataAccessLayer
          .persistCredentials(credentialsViewModel.credentialsModel)).called(1);
    });
    expect(
        credentialsViewModel.password ==
            credentialsViewModel.credentialsModel.password,
        false);
    expect(credentialsViewModel.masterPassword,
        credentialsViewModel.credentialsModel.masterPassword);
  });

  test('load credentials', () {
    clearInteractions(dataAccessLayer);
    CredentialsViewModel credentialsViewModel =
        CredentialsViewModel(dataAccessLayer: dataAccessLayer);
    when(credentialsViewModel.loadModel()).thenAnswer((_) async {
      verify(() => dataAccessLayer.loadConfig()).called(1);
    });
  });

  test('handle logout', () {
    CredentialsViewModel credentialsViewModel =
        CredentialsViewModel(dataAccessLayer: dataAccessLayer);
    credentialsViewModel.masterPassword = 'testValue';
    expect(
        credentialsViewModel.masterPassword ==
            credentialsViewModel.credentialsModel.masterPassword,
        false);
    credentialsViewModel.handleLogout();
    expect(credentialsViewModel.masterPassword,
        credentialsViewModel.credentialsModel.masterPassword);
  });
}
