// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/widgets/folder_tree.dart';

Widget passwordFolderScreen(BuildContext context) {
  return context.watch<PasswordListViewModel>().passwordViewModels.isEmpty ||
          context.watch<ViewState>().hideData
      ? const CircularProgressIndicator()
      : createTree(context);
}
