// Package imports:
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';

// Project imports:
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';
import 'tag_view_model_test.mocks.dart';

@GenerateMocks([DataAccessLayer])
void main() {
  MockDataAccessLayer dataAccessLayer = MockDataAccessLayer();
  TagViewModel tagViewModelLocal = TagViewModel();
  tagViewModelLocal.id = '3fe254fa-f60d-48a0-bc53-5523146dac1f';
  tagViewModelLocal.revision = 'df06ed08-d110-4157-96f1-856a65c50885';
  tagViewModelLocal.label = 'Messenger';
  tagViewModelLocal.color = '#cddc39';
  tagViewModelLocal.edited = 1507408874;
  tagViewModelLocal.favorite = false;
  tagViewModelLocal.refreshModel(
      fromModel: tagViewModelLocal, toModel: tagViewModelLocal.tagModel);
  test('fromMap', () {
    TagViewModel tagViewModel =
        TagViewModel.fromMap(dataAccessLayer.samples['tags'][6]);
    expect(tagViewModelLocal, tagViewModel);
  });
  test('fromModel', () {
    TagViewModel tagViewModel = TagViewModel.fromModel(tagViewModelLocal);
    expect(tagViewModelLocal, tagViewModel);
  });
  test('refreshTagModel', () {
    tagViewModelLocal.client = 'test';
    expect(tagViewModelLocal == tagViewModelLocal.tagModel, false);
    tagViewModelLocal.refreshModel(
        fromModel: tagViewModelLocal, toModel: tagViewModelLocal.tagModel);
    expect(tagViewModelLocal.props, tagViewModelLocal.tagModel.props);
  });
  test('refreshTagViewModel', () {
    tagViewModelLocal.tagModel.hidden = true;
    expect(tagViewModelLocal == tagViewModelLocal.tagModel, false);
    tagViewModelLocal.refreshModel(
        fromModel: tagViewModelLocal.tagModel, toModel: tagViewModelLocal);
    expect(tagViewModelLocal.props, tagViewModelLocal.tagModel.props);
  });
}
