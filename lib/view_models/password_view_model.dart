// Dart imports:
// ignore_for_file: use_build_context_synchronously

// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/interfaces/password_objects_interface.dart';
import 'package:nextcloud_password_client/models/password_model.dart';
import 'package:nextcloud_password_client/models/password_share_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_share_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class PasswordViewModel extends PasswordModel
    implements Comparable<PasswordViewModel>, PasswordObjectsInterface {
  DataAccessLayer dataAccessLayer;
  List<PasswordViewModel> revisionViewModels = [];
  List<PasswordShareViewModel> shareViewModels = [];
  PasswordModel _passwordModel = PasswordModel();
  PasswordShareViewModel passwordShareView = PasswordShareViewModel();

  PasswordViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();
  PasswordModel get passwordModel => _passwordModel;

  PasswordViewModel.fromModel(PasswordModel model,
      {DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer() {
    _passwordModel = PasswordModel();
    refreshViewModel(model);
    refreshDataModel(this);
  }

  PasswordViewModel.fromMap(Map<String, dynamic> password,
      {DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer() {
    _passwordModel.id = id = password['id'] ?? id;
    _passwordModel.label = label = password['label'] ?? label;
    _passwordModel.username = username = password['username'] ?? username;
    _passwordModel.password = this.password = password['password'] ?? password;
    _passwordModel.url = url = password['url'] ?? url;
    _passwordModel.notes = notes = password['notes'] ?? notes;
    if (password['customFields'] != null) {
      _passwordModel.customFields =
          customFields = password['customFields'].toString();
    }
    _passwordModel.status = status = password['status'] ?? status;
    _passwordModel.statusCode =
        statusCode = password['statusCode'] ?? statusCode;
    _passwordModel.hash = hash = password['hash'] ?? hash;
    _passwordModel.folder = folder = password['folder'] ?? folder;
    _passwordModel.revision = revision = password['revision'] ?? revision;
    if (password['share'] != null) {
      passwordShareView = PasswordShareViewModel.fromMap(password['share']);
      _passwordModel.share = passwordShareView.passwordShareModel;
    }
    _passwordModel.shared = shared = password['shared'] ?? shared;
    _passwordModel.cseType = cseType = password['cseType'] ?? cseType;
    _passwordModel.cseKey = cseKey = password['cseKey'] ?? cseKey;
    _passwordModel.sseType = sseType = password['sseType'] ?? sseType;
    _passwordModel.client = client = password['client'] ?? client;
    _passwordModel.hidden = hidden = password['hidden'] ?? hidden;
    _passwordModel.trashed = trashed = password['trashed'] ?? trashed;
    _passwordModel.favorite = favorite = password['favorite'] ?? favorite;
    _passwordModel.editable = editable = password['editable'] ?? editable;
    _passwordModel.edited = edited = password['edited'] ?? edited;
    _passwordModel.created = created = password['created'] ?? created;
    _passwordModel.updated = updated = password['updated'] ?? updated;
    if (password['revisions'] != null) {
      revisionViewModels =
          getRevisionsFromAPI(List.castFrom(password['revisions']));
      _passwordModel.revisions = getRevisionModelFromViewModels();
    }
    if (password['tags'] != null) {
      _passwordModel.tags = tags = getTagIdsFromAPI(password['tags']);
    }
    if (password['shares'] != null) {
      shareViewModels = getSharesFromAPI(password['shares']);
      _passwordModel.shares = getShareModelsFromViewModels();
    }
  }
  void refreshModel(
      {required PasswordModel fromModel, required PasswordModel toModel}) {
    toModel.favIconString = fromModel.favIconString;
    toModel.id = fromModel.id;
    toModel.label = fromModel.label;
    toModel.username = fromModel.username;
    toModel.password = fromModel.password;
    toModel.url = fromModel.url;
    toModel.notes = fromModel.notes;
    toModel.customFields = fromModel.customFields;
    toModel.status = fromModel.status;
    toModel.statusCode = fromModel.statusCode;
    toModel.hash = fromModel.hash;
    toModel.folder = fromModel.folder;
    toModel.revision = fromModel.revision;
    toModel.share = fromModel.share;
    toModel.shared = fromModel.shared;
    toModel.cseType = fromModel.cseType;
    toModel.cseKey = fromModel.cseKey;
    toModel.sseType = fromModel.sseType;
    toModel.client = fromModel.client;
    toModel.hidden = fromModel.hidden;
    toModel.trashed = fromModel.trashed;
    toModel.favorite = fromModel.favorite;
    toModel.editable = fromModel.editable;
    toModel.edited = fromModel.edited;
    toModel.created = fromModel.created;
    toModel.updated = fromModel.updated;
    toModel.tags = fromModel.tags;
  }

  void refreshViewModel(PasswordModel passwordModel) {
    refreshModel(fromModel: passwordModel, toModel: this);
    revisionViewModels = getRevisionViewModelsFromModels(_passwordModel);
    shareViewModels = getShareViewModelFromModels(_passwordModel);
    passwordShareView = PasswordShareViewModel.fromModel(_passwordModel.share);
    notifyListeners();
  }

  void refreshDataModel(PasswordViewModel passwordViewModel) {
    refreshModel(fromModel: passwordViewModel, toModel: _passwordModel);
    _passwordModel.revisions = getRevisionModelFromViewModels();
    _passwordModel.shares = getShareModelsFromViewModels();
    notifyListeners();
  }

  @override
  Future<String> setFavorite(BuildContext context, bool newFavorite) async {
    favorite = newFavorite;
    _passwordModel.favorite = favorite;
    dataAccessLayer.savePassword(_passwordModel);
    notifyListeners();
    http.Response passwordUpdateResponse = await HttpUtils()
        .httpUpdate(apiPatchPasswordUpdate, body: getBodyForFavoriteUpdate());
    if (passwordUpdateResponse.statusCode == 200) {
      var body = json.decode(passwordUpdateResponse.body);
      PasswordViewModel revision = PasswordViewModel.fromModel(this);
      revision.id = body['revision'];
      revision.favorite = !revision.favorite;
      revisionViewModels.add(revision);
      revisions.add(revision._passwordModel);
      notifyListeners();
      return "";
    } else {
      return AppLocalizations.of(context)!.passwordRemoteUpdateError + AppLocalizations.of(context)!.localChangesOverwriteHint;
    }

  }

  List<PasswordViewModel> getRevisionsFromAPI(
      List<Map<String, dynamic>> revisions) {
    List<PasswordViewModel> models = [];
    for (Map<String, dynamic> revision in revisions) {
      models.add(PasswordViewModel.fromMap(revision));
    }
    return models;
  }

  List<PasswordModel> getRevisionModelFromViewModels() {
    List<PasswordModel> models = [];
    for (PasswordViewModel revision in revisionViewModels) {
      models.add(revision._passwordModel);
    }
    return models;
  }

  List<PasswordViewModel> getRevisionViewModelsFromModels(
      PasswordModel passwordModel) {
    List<PasswordViewModel> models = [];
    for (PasswordModel revision in passwordModel.revisions) {
      models.add(PasswordViewModel.fromModel(revision));
    }
    return models;
  }

  List<String> getTagIdsFromAPI(tags) {
    List<String> tagList = [];
    for (String tag in tags) {
      tagList.add(tag);
    }
    return tagList;
  }

  List<PasswordShareViewModel> getSharesFromAPI(shares) {
    List<PasswordShareViewModel> models = [];
    for (Map<String, dynamic> share in shares) {
      models.add(PasswordShareViewModel.fromMap(share));
    }
    return models;
  }

  List<PasswordShareModel> getShareModelsFromViewModels() {
    List<PasswordShareModel> models = [];
    for (PasswordShareViewModel share in shareViewModels) {
      models.add(share.passwordShareModel);
    }
    return models;
  }

  List<PasswordShareViewModel> getShareViewModelFromModels(
      PasswordModel passwordModel) {
    List<PasswordShareViewModel> models = [];
    for (PasswordShareModel share in passwordModel.shares) {
      models.add(PasswordShareViewModel.fromModel(share));
    }
    return models;
  }

  @override
  int compareTo(PasswordViewModel other) {
    if (edited < other.edited) {
      return 1;
    } else if (edited > other.edited) {
      return -1;
    } else {
      return 0;
    }
  }

  String getBodyForFavoriteUpdate() {
    return jsonEncode({
      'id': id,
      'password': password,
      'label': label,
      'hash': hash,
      'cseType': cseType,
      'cseKey': cseKey,
      'favorite': favorite
    });
  }

  Future<void> deleteTag(
      BuildContext context, TagViewModel tagViewModel) async {
    tags.remove(tagViewModel.id);
    _passwordModel.tags = tags;
    dataAccessLayer.savePassword(_passwordModel);
    notifyListeners();
    context.read<PasswordListViewModel>().notifyListeners();
    http.Response passwordUpdateResponse = await HttpUtils()
        .httpUpdate(apiPatchPasswordUpdate, body: getBodyForTagUpdate());
    if (passwordUpdateResponse.statusCode == 200) {
      var body = json.decode(passwordUpdateResponse.body);
      PasswordViewModel revision = PasswordViewModel.fromModel(this);
      revision.id = body['revision'];
      revisionViewModels.add(revision);
      revisions.add(revision._passwordModel);
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError + AppLocalizations.of(context)!.localChangesOverwriteHint,
          duration: const Duration(seconds: 5), context: context);
    }

    notifyListeners();
  }

  String getBodyForTagUpdate() {
    return jsonEncode({
      'id': id,
      'password': password,
      'label': label,
      'hash': hash,
      'cseType': cseType,
      'cseKey': cseKey,
      'tags': tags.isEmpty ? [''] : tags
    });
  }

  Future<void> addTag(BuildContext context, String newTag) async {
    context.read<ViewState>().inEditMode = true;
    KeyChain keyChain = context.read<ConfigViewModel>().keyChain;
    PasswordListViewModel passwordList = context.read<PasswordListViewModel>();
    TagListViewModel tagListViewModel = context.read<TagListViewModel>();
    String encryptedTag =
        keyChain.encrypt(newTag, keyChain.type, keyChain.current);
    TagViewModel tag = tagListViewModel.getTagByLabel(keyChain, newTag);
    if (tag.id.isNotEmpty) {
      tags.add(tag.id);
      passwordList.notifyListeners();
      notifyListeners();
      updatePassword(context);
    } else {
      tag.label = encryptedTag;
      tag.cseType = keyChain.type;
      tag.cseKey = keyChain.current;
      tag.color = keyChain.encrypt(tag.color, tag.cseType, tag.cseKey);
      tag.id = UniqueKey().hashCode.toString();
      tag.refreshModel(fromModel: tag, toModel: tag.tagModel);
      tagListViewModel.addTag(tag);
      tags.add(tag.id);
      passwordList.notifyListeners();
      notifyListeners();
      dataAccessLayer.savePassword(_passwordModel);
      tagListViewModel.createTag(tag).then((answerTag) {
        tags.remove(tag.id);
        tags.add(answerTag.id);
        tagListViewModel.deleteTag(tag);
        tagListViewModel.addTag(answerTag);
        updatePassword(context);
        notifyListeners();
      });
    }
  }

  Future<void> updatePassword(BuildContext context) async {
    http.Response response = await HttpUtils()
        .httpUpdate(apiPatchPasswordUpdate, body: getBodyForPasswordUpdate());
    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      revision = body["revision"];
      context
          .read<PasswordListViewModel>()
          .reassignPasswordToFolder(context, this);
      notifyListeners();
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError + AppLocalizations.of(context)!.localChangesOverwriteHint,
          duration: const Duration(seconds: 5), context: context);
    }
    context.read<ViewState>().inEditMode = false;
    dataAccessLayer.savePassword(_passwordModel);
  }

  Future<void> restorePassword(BuildContext context, String revisionID) async {
    http.Response response = await HttpUtils().httpUpdate(
        apiPatchPasswordRestore,
        body: getBodyForPasswordRestore(revisionID));
    if (response.statusCode == 200) {
      var body = json.decode(response.body);
      handleNewRevision(context, body["revision"], revisionID);
      context
          .read<PasswordListViewModel>()
          .reassignPasswordToFolder(context, this);
      notifyListeners();
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError + AppLocalizations.of(context)!.localChangesOverwriteHint,
          duration: const Duration(seconds: 5), context: context);
    }
    dataAccessLayer.savePassword(_passwordModel);
  }

  String getBodyForPasswordUpdate() {
    return jsonEncode({
      'id': id,
      'label': label,
      'username': username,
      'password': password,
      'revision': revision,
      'url': url,
      'notes': notes,
      'customFields': customFields,
      'status': status,
      'statusCode': statusCode,
      'hash': hash,
      'folder': folder,
      'cseType': cseType,
      'cseKey': cseKey,
      'sseType': sseType,
      'client': client,
      'hidden': hidden,
      'trashed': trashed,
      'favorite': favorite,
      'edited': edited,
      'updated': updated,
      'created': created,
      'tags': tags
    });
  }

  String getBodyForPasswordRestore(String revisionId) {
    return jsonEncode({
      'id': id,
      'revision': revisionId,
    });
  }

  void handleNewRevision(
      BuildContext context, String newRevisionID, oldRevisionID) {
    PasswordViewModel oldPassword = PasswordViewModel.fromModel(this);
    PasswordViewModel restoredPassword = PasswordViewModel();
    for (List<PasswordViewModel> passwords
        in context.read<PasswordListViewModel>().passwordViewModels.values) {
      restoredPassword =
          passwords.firstWhere((element) => element.revision = oldRevisionID);
      restoredPassword._passwordModel.revision =
          restoredPassword.revision = newRevisionID;
    }

    refreshViewModel(restoredPassword);
    revisionViewModels.add(oldPassword);
    _passwordModel.revisions.add(oldPassword._passwordModel);
    notifyListeners();
  }

  void createPassword(BuildContext context) async {
    http.Response response = await HttpUtils()
        .httpPost(apiPostPasswordCreate, body: getBodyForPasswordCreation());
    if (response.statusCode == 201) {
      var body = json.decode(response.body);
      id = body["id"];
      revision = body["revision"];
      refreshDataModel(this);
      context
          .read<PasswordListViewModel>()
          .reassignPasswordToFolder(context, this);
      notifyListeners();
    } else {
      showToast(AppLocalizations.of(context)!.passwordRemoteUpdateError + AppLocalizations.of(context)!.localChangesOverwriteHint,
          duration: const Duration(seconds: 5), context: context);
    }
    context.read<ViewState>().inEditMode = false;
    dataAccessLayer.savePassword(_passwordModel);
  }

  String getBodyForPasswordCreation() {
    return jsonEncode({
      'label': label,
      'username': username,
      'password': password,
      'url': url,
      'notes': notes,
      'customFields': customFields,
      'hash': hash,
      'folder': folder,
      'cseType': cseType,
      'cseKey': cseKey,
      'sseType': sseType,
      'hidden': hidden,
      'favorite': favorite,
      'edited': edited,
      'tags': tags
    });
  }
}
