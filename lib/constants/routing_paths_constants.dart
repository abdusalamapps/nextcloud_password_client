const routingPathLogin = '/login';
const routingPathSettings = '/settings';
const routingPathPasswordScreen = '/passwordScreen';
const routingPathMissingDependenciesScreen = '/missingDependenciesScreen';
