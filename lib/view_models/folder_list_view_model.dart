// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/folder_model.dart';
import 'package:nextcloud_password_client/provider/nextcloud_folder_provider.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:nextcloud_password_client/view_models/folder_view_model.dart';

class FolderListViewModel extends ChangeNotifier implements ViewModelInterface {
  Map<String, List<FolderViewModel>> _folderViewModels = {};
  DataAccessLayer dataAccessLayer;

  FolderListViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  Map<String, List<FolderViewModel>> get folderViewModels => _folderViewModels;

  set folderViewModels(Map<String, List<FolderViewModel>> folderViewModels) {
    _folderViewModels = folderViewModels;
    notifyListeners();
  }

  Map<String, List<FolderViewModel>> getFolderViewModels() => _folderViewModels;

  @override
  Future<void> initialize() async {
    loadModel();
  }

  @override
  void loadModel() {
    List<FolderModel> models = dataAccessLayer.loadFolder();
    for (FolderModel folder in models) {
      addFolderToList(folder);
    }
    notifyListeners();
  }

  void addFolderToList(FolderModel folder) {
    if (!_folderViewModels.containsKey(folder.parent)) {
      _folderViewModels[folder.parent] = [];
    }
    _folderViewModels[folder.parent]!.add(FolderViewModel.fromModel(folder));
  }

  @override
  Future<void> persistModel(
      [bool firstBool = false, bool secondBool = false]) async {
    List<FolderModel> models = [];
    for (List<FolderModel> folder in _folderViewModels.values) {
      models.addAll(folder);
    }
    dataAccessLayer.persistFolder(models);
  }

  FolderViewModel getFolderById(String folderID) {
    FolderViewModel model = FolderViewModel();
    if (_folderViewModels.containsKey(folderID)) {}
    _folderViewModels.forEach((key, value) {
      for (var folderViewModel in value) {
        if (folderViewModel.id == folderID) {
          model = folderViewModel;
        }
      }
    });
    return model;
  }

  @override
  void handleLogout() {
    _folderViewModels.clear();
    notifyListeners();
  }

  List<FolderViewModel> getFavoriteFolder() {
    List<FolderViewModel> models = [];
    for (List<FolderViewModel> folderViewModels in _folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        if (folderViewModel.favorite) {
          models.add(folderViewModel);
        }
      }
    }
    return models;
  }

  FolderViewModel getDirectParent(BuildContext context, String folderID) {
    FolderViewModel parentFolder = FolderViewModel();
    parentFolder.id = apiRootFolder;
    parentFolder.label = AppLocalizations.of(context)!.all;
    for (List<FolderViewModel> folders in _folderViewModels.values) {
      for (FolderViewModel folder in folders) {
        if (folder.id == folderID) {
          parentFolder = folder;
          break;
        }
      }
    }
    return parentFolder;
  }

  List<String> getParentFolders(String selectedFolder) {
    List<String> parents = [];
    for (List<FolderViewModel> folders in _folderViewModels.values) {
      for (FolderViewModel folder in folders) {
        if (folder.id == selectedFolder) {
          parents.addAll(getParentFolders(folder.parent));
          parents.add(selectedFolder);
        }
      }
    }
    return parents;
  }

  @override
  void refreshDataModels() {
    for (List<FolderViewModel> folderViewModels in _folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        folderViewModel.refreshDataModel(folderViewModel);
      }
    }
  }

  @override
  void refreshViewModels() {
    for (List<FolderViewModel> folderViewModels in _folderViewModels.values) {
      for (FolderViewModel folderViewModel in folderViewModels) {
        folderViewModel.refreshViewModel(folderViewModel.folderModel);
      }
    }
  }

  void delete(BuildContext context,
      {String? folderId, FolderViewModel? folder}) async {
    late FolderViewModel folderViewModel;

    if (folderId != null) {
      folderViewModel = getFolderById(folderId);
    } else if (folder != null) {
      folderViewModel = folder;
    } else {
      return;
    }

    if (folderViewModel.id == apiRootFolder) {
      showToast(AppLocalizations.of(context)!.rootFolderDeletionError,
          duration: const Duration(seconds: 5), context: context);
      return;
    }

    NextcloudFolderProvider.deleteFolder(jsonEncode({'id': folderViewModel.id}))
        .then((folderDeleted) => {
              if (!folderDeleted)
                {
                  showToast(
                      AppLocalizations.of(context)!.folderRemoteDeletionError +
                          AppLocalizations.of(context)!
                              .localChangesOverwriteHint,
                      duration: const Duration(seconds: 5),
                      context: context)
                }
            });

    removeFolder(folderViewModel);
    notifyListeners();
  }

  void removeFolder(FolderViewModel folderViewModel) {
    for (List<FolderViewModel> folders in _folderViewModels.values) {
      if (folders.contains(folderViewModel)) {
        folders.remove(folderViewModel);
      }
    }
  }

  void createFolder(BuildContext context, FolderViewModel newFolder) async {
    newFolder.id = UniqueKey().hashCode.toString();
    FolderViewModel newFolderTemp = FolderViewModel.fromModel(newFolder);
    if (_folderViewModels.containsKey(newFolder.parent)) {
      _folderViewModels[newFolder.parent]!.add(newFolderTemp);
    } else {
      _folderViewModels[newFolder.parent] = [newFolderTemp];
    }
    notifyListeners();
    newFolder.createFolder().then((folderCreated) => {
          if (folderCreated)
            {
              _folderViewModels[newFolder.parent]!.remove(newFolderTemp),
              _folderViewModels[newFolder.parent]!.add(newFolder),
              notifyListeners()
            }
          else
            {
              showToast(
                  AppLocalizations.of(context)!.passwordRemoteUpdateError +
                      AppLocalizations.of(context)!.localChangesOverwriteHint,
                  duration: const Duration(seconds: 5),
                  context: context)
            }
        });
  }

  void updateFolder(BuildContext context, FolderViewModel folder) async {
    notifyListeners();
    folder.updateFolder().then((folderUpdated) => {
          if (!folderUpdated)
            {
              showToast(
                  AppLocalizations.of(context)!.passwordRemoteUpdateError +
                      AppLocalizations.of(context)!.localChangesOverwriteHint,
                  duration: const Duration(seconds: 5),
                  context: context)
            }
        });
  }
}
