// Dart imports:
// ignore_for_file: use_build_context_synchronously

// Dart imports:
import 'dart:convert';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/tag_model.dart';
import 'package:nextcloud_password_client/provider/nextcloud_tag_provider.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/tag_view_model.dart';

class TagListViewModel extends ChangeNotifier implements ViewModelInterface {
  List<TagViewModel> _tagViewModels = [];
  DataAccessLayer dataAccessLayer;
  TagListViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  List<TagViewModel> get tagViewModels => _tagViewModels;

  set tagViewModels(List<TagViewModel> tagViewModels) {
    _tagViewModels = tagViewModels;
    notifyListeners();
  }

  @override
  Future<void> initialize() async {
    loadModel();
  }

  @override
  void loadModel() {
    List<TagModel> models = dataAccessLayer.loadTags();
    for (TagModel tag in models) {
      tagViewModels.add(TagViewModel.fromModel(tag));
    }
    notifyListeners();
  }

  @override
  Future<void> persistModel(
      [bool firstBool = false, bool secondBool = false]) async {
    List<TagModel> models = [];
    for (TagViewModel tag in tagViewModels) {
      models.add(tag.tagModel);
    }
    dataAccessLayer.persistTags(models);
  }

  TagViewModel getTagByLabel(KeyChain keyChain, String newTagLabel) {
    TagViewModel model = TagViewModel();
    for (TagViewModel tag in _tagViewModels) {
      String tagLabel = keyChain.decrypt(tag.cseKey, tag.label);
      if (tagLabel == newTagLabel) {
        model = tag;
        break;
      }
    }
    return model;
  }

  TagViewModel getTagById(String tagID) {
    TagViewModel model = TagViewModel();
    for (TagViewModel tag in _tagViewModels) {
      if (tag.id == tagID) {
        model = tag;
        break;
      }
    }
    return model;
  }

  @override
  void handleLogout() {
    _tagViewModels.clear();
  }

  List<TagViewModel> getFavoriteTags() {
    List<TagViewModel> tagViewModels = [];
    for (TagViewModel tagViewModel in _tagViewModels) {
      if (tagViewModel.favorite) {
        tagViewModels.add(tagViewModel);
      }
    }
    return tagViewModels;
  }

  Future<TagViewModel> createTag(TagViewModel tag) async {
    TagViewModel newTag = TagViewModel();
    http.Response tagCreationResponse = await HttpUtils()
        .httpPost(apiPostTagCreate, body: getBodyForTagCreation(tag));
    if (tagCreationResponse.statusCode == 201) {
      var body = jsonDecode(tagCreationResponse.body);
      newTag = await showTag(body['id']);
      if (newTag.id == body['id']) {
        _tagViewModels.add(newTag);
        persistModel();
        notifyListeners();
      }
    }
    return newTag;
  }

  String getBodyForTagCreation(TagViewModel tag) {
    return jsonEncode({
      'label': tag.label,
      'cseType': tag.cseType,
      'cseKey': tag.cseKey,
      'color': tag.color
    });
  }

  Future<TagViewModel> showTag(String tagId) async {
    TagViewModel newTag = TagViewModel();
    http.Response tagShowResponse = await HttpUtils()
        .httpPost(apiPostTagShow, body: jsonEncode({"id": tagId}));
    if (tagShowResponse.statusCode == 200) {
      newTag = TagViewModel.fromMap(jsonDecode(tagShowResponse.body));
    }
    return newTag;
  }

  void addTag(TagViewModel tag) {
    _tagViewModels.add(tag);
    notifyListeners();
  }

  void deleteTag(TagViewModel tag) {
    _tagViewModels.remove(tag);
    notifyListeners();
  }

  @override
  void refreshDataModels() {
    for (TagViewModel tagViewModel in _tagViewModels) {
      tagViewModel.refreshModel(
          fromModel: tagViewModel, toModel: tagViewModel.tagModel);
    }
  }

  @override
  void refreshViewModels() {
    for (TagViewModel tagViewModel in _tagViewModels) {
      tagViewModel.refreshModel(
          fromModel: tagViewModel.tagModel, toModel: tagViewModel);
    }
  }

  delete(BuildContext context, {String? tagId, TagViewModel? tag}) async {
    TagViewModel tagViewModel = TagViewModel();

    if (tagId != null) {
      tagViewModel = getTagById(tagId);
    } else if (tag != null) {
      tagViewModel = tag;
    } else {
      return;
    }
    NextcloudTagProvider.deleteTag(jsonEncode({'id': tagViewModel.id}));
    
    removeTag(tagViewModel);
    notifyListeners();
  }

  void removeTag(TagViewModel tagViewModel) {
    _tagViewModels.remove(tagViewModel);
  }

  void updateTag(TagViewModel tag) async {
    http.Response tagUpdateResponse = await HttpUtils()
        .httpUpdate(apiPatchTagUpdate, body: getBodyForTagUpdate(tag));
    if (tagUpdateResponse.statusCode == 200) {}
  }

  String getBodyForTagUpdate(TagViewModel tag) {
    return jsonEncode({
      'label': tag.label,
      'cseType': tag.cseType,
      'cseKey': tag.cseKey,
      'color': tag.color,
      'favorite': tag.favorite
    });
  }
}
