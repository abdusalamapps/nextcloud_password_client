// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';

part 'folder_model.g.dart';

@HiveType(typeId: 3)
class FolderModel extends BaseModel {
  @HiveField(13)

  /// UUID of the parent folder
  String parent = '';

  @HiveField(14)
  List<FolderModel> revisions = [];

  @override
  List<Object?> get props => super.props..addAll([parent, revision]);
  FolderModel();
}
