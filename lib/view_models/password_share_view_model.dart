// Project imports:
import 'dart:convert';

import 'package:http/src/response.dart';
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/models/password_share_model.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';

class PasswordShareViewModel extends PasswordShareModel {
  PasswordShareModel _passwordShareModel = PasswordShareModel();

  PasswordShareViewModel.fromModel(PasswordShareModel model) {
    _passwordShareModel = PasswordShareModel();
    refreshModel(fromModel: model, toModel: _passwordShareModel);
    refreshModel(fromModel: model, toModel: this);
  }
  PasswordShareViewModel();
  PasswordShareModel get passwordShareModel => _passwordShareModel;
  PasswordShareViewModel.fromMap(Map<String, dynamic> share) {
    _passwordShareModel.id = id = share['id'] ?? id;
    _passwordShareModel.expires = expires = share['expires'] ?? expires;
    _passwordShareModel.editable = editable = share['editable'] ?? editable;
    _passwordShareModel.password = password = share['password'] ?? password;
    _passwordShareModel.shareable = shareable = share['shareable'] ?? shareable;
    _passwordShareModel.updatePending =
        updatePending = share['updatePending'] ?? updatePending;
    _passwordShareModel.owner = owner = share['owner'] ?? owner;
    _passwordShareModel.receiver = receiver = share['receiver'] ?? receiver;
    _passwordShareModel.created = created = share['created'] ?? created;
    _passwordShareModel.updated = updated = share['updated'] ?? updated;
  }

  void refreshModel(
      {required PasswordShareModel fromModel,
      required PasswordShareModel toModel}) {
    toModel.id = fromModel.id;
    toModel.password = fromModel.password;
    toModel.created = fromModel.created;
    toModel.updated = fromModel.updated;
    toModel.expires = fromModel.expires;
    toModel.editable = fromModel.editable;
    toModel.shareable = fromModel.shareable;
    toModel.updatePending = fromModel.updatePending;
    toModel.owner = fromModel.owner;
    toModel.receiver = fromModel.receiver;
    notifyListeners();
  }

  updateEditable() async{
    Future<Response> response = HttpUtils().httpUpdate(apiShareUpdate, body: jsonEncode({'id': id, 'editable':!editable}));
  }
  updateShareable() async{
    Future<Response> response = HttpUtils().httpUpdate(apiShareUpdate, body: jsonEncode({'id':id, 'editable': !shareable}));
  }
  updateExpiration({int? timestamp} ) async{
    Future<Response> response = HttpUtils().httpUpdate(apiShareUpdate, body: jsonEncode({'id': id}));
  }
  deleteShare( ) async{
    Future<Response> response = HttpUtils().httpDelete(apiShareDelete, body: jsonEncode({'id': id}));
  }

}
