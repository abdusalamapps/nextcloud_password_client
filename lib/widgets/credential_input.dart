// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/controller/textfield_controller.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';
import 'package:nextcloud_password_client/widgets/single_elements/general_text_input.dart';
import 'package:nextcloud_password_client/widgets/single_elements/label.dart';
import 'package:nextcloud_password_client/widgets/single_elements/password_input.dart';

Widget credentialInput(BuildContext context) {
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: _userNameRow(context)),
      Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: _passwordRow(context)),
      Container(
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: _saveCredentialsRow(context))
    ],
  );
}

Widget _userNameRow(BuildContext context) {
  return getGeneralTextInput(TextFieldController.userNameFieldController,
      label: AppLocalizations.of(context)!.userName, onChanged: (value) {
    context.read<ViewState>().credentialsProvided = value.isNotEmpty &&
        TextFieldController.passwordFieldController.text.isNotEmpty;
  },
      error: context.watch<ViewState>().credentialInputError
          ? AppLocalizations.of(context)!.credentialInputException
          : null);
}

Widget _passwordRow(BuildContext context) {
  return PasswordText(TextFieldController.passwordFieldController,
      labelText: AppLocalizations.of(context)!.password, onChanged: (value) {
    context.read<ViewState>().credentialsProvided = value.isNotEmpty &&
        TextFieldController.userNameFieldController.text.isNotEmpty;
  },
      errorText: context.read<ViewState>().credentialInputError
          ? AppLocalizations.of(context)!.credentialInputException
          : null);
}

Widget _saveCredentialsRow(BuildContext context) {
  return Row(
    children: <Widget>[
      Expanded(child: Label(AppLocalizations.of(context)!.saveCredentials)),
      _saveCredentialCheckBox(context)
    ],
  );
}

Widget _saveCredentialCheckBox(BuildContext context) {
  CredentialsViewModel credentialsViewModel =
      context.read<CredentialsViewModel>();
  bool saveCredentials = context.watch<ConfigViewModel>().saveCredentials;
  return Checkbox(
    checkColor: Colors.white,
    value: saveCredentials,
    onChanged: (value) {
      credentialsViewModel.userName =
          TextFieldController.userNameFieldController.text;
      credentialsViewModel.password =
          TextFieldController.passwordFieldController.text;
      context.read<ConfigViewModel>().saveCredentials = !saveCredentials;
    },
  );
}
