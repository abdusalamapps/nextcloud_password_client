// Dart imports:

// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/interfaces/view_model_interface.dart';
import 'package:nextcloud_password_client/models/user_model.dart';
import 'package:nextcloud_password_client/utils/data_access_layer.dart';
import 'package:nextcloud_password_client/view_models/user_view_model.dart';

class UserListViewModel extends ChangeNotifier implements ViewModelInterface {
  List<UserViewModel> _userViewModels = [];
  DataAccessLayer dataAccessLayer;

  UserListViewModel({DataAccessLayer? dataAccessLayer})
      : dataAccessLayer = dataAccessLayer ?? DataAccessLayer();

  List<UserViewModel> get userViewModels => _userViewModels;

  set userViewModels(List<UserViewModel> userViewModels) {
    _userViewModels = userViewModels;
    notifyListeners();
  }

  List<UserViewModel> getUserViewModels() => _userViewModels;

  @override
  Future<void> initialize() async {
    loadModel();
  }

  @override
  void loadModel() {
    List<UserModel> models = dataAccessLayer.loadUser();
    for (UserModel user in models) {
      _userViewModels.add(UserViewModel.fromModel(user));
    }
    notifyListeners();
  }

  @override
  Future<void> persistModel(
      [bool firstBool = false, bool secondBool = false]) async {
    List<UserModel> models = [];
    for (UserViewModel user in _userViewModels) {
      models.add(user.userModel);
    }
    dataAccessLayer.persistUsers(models);
  }

  UserViewModel getuserById(String userID) {
    UserViewModel model = UserViewModel();
      for (UserViewModel userViewModel in _userViewModels) {
        if (userViewModel.id == userID) {
          model = userViewModel;
        }
      }
    return model;
  }

  @override
  void handleLogout() {
    _userViewModels.clear();
    notifyListeners();
  }

 

  @override
  void refreshDataModels() {
      for (UserViewModel userViewModel in _userViewModels) {
        userViewModel.refreshModel(
            fromModel: userViewModel, toModel: userViewModel.userModel);
      }
    }
  

  @override
  void refreshViewModels() {
      for (UserViewModel userViewModel in _userViewModels) {
        userViewModel.refreshModel(
            fromModel: userViewModel.userModel, toModel: userViewModel);
      }
    }
    }
