// Package imports:
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'dart:math' as math;

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';

part 'tag_model.g.dart';

@HiveType(typeId: 9)
class TagModel extends BaseModel {
  /// The color of the tag. Any valid CSSv3 color is accepted.
  @HiveField(13)
  String color = '0xFFFFFF';

    ///list of all revisions
  @HiveField(14)
  List<TagModel> revisions = [];

  TagModel() {
    String tempColor = Color((math.Random().nextDouble() * 0xFFFFFF).toInt())
        .withOpacity(1.0)
        .toString();
    tempColor = tempColor.replaceRange(0, 10, '#');
    tempColor = tempColor.replaceFirst(')', '');
    tempColor = tempColor.replaceFirst(')', '');
    color = tempColor;
  }

  @override
  List<Object?> get props => super.props..addAll([color]);
}
