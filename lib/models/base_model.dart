// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/interfaces/password_objects_interface.dart';

part 'base_model.g.dart';

@HiveType(typeId: 13)
class BaseModel extends PasswordObjectsInterface with EquatableMixin {
  @HiveField(0)

  /// The UUID of the object
  String id = '';
  @HiveField(1)

  /// User defined label of the object
  String label = '';
  @HiveField(2)

  /// UUID of the current revision
  String revision = '';
  @HiveField(3)

  /// Type of the used client side encryption
  String cseType = '';
  @HiveField(4)

  /// UUID of the key used for client side encryption
  String cseKey = '';
  @HiveField(5)

  /// Type of the used server side encryption
  String sseType = '';
  @HiveField(6)

  /// Name of the client which created this revision
  String client = '';
  @HiveField(7)

  /// Hides the folder in list / find actions
  bool hidden = false;
  @HiveField(8)

  /// True if the folder is in the trash
  bool trashed = false;
  @HiveField(9)

  /// True if the user has marked the folder as favorite
  bool favorite = false;
  @HiveField(10)

  /// Unix timestamp when the folder was created
  int created = 0;
  @HiveField(11)

  /// Unix timestamp when the folder was updated
  int updated = 0;
  @HiveField(12)

  /// Unix timestamp when the user last changed the folder name
  int edited = 0;

  @override
  List<Object?> get props => [
        id,
        label,
        revision,
        cseType,
        cseKey,
        sseType,
        client,
        hidden,
        trashed,
        favorite,
        created,
        updated
      ];
}
