// Dart imports:
import 'dart:core';

// Package imports:
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/models/base_model.dart';
import 'package:nextcloud_password_client/models/password_share_model.dart';

part 'password_model.g.dart';

@HiveType(typeId: 4)
class PasswordModel extends BaseModel {
  @HiveField(13)

  /// Username associated with the password
  String username = '';
  @HiveField(14)

  /// The actual password
  String password = '';
  @HiveField(15)

  ///Url of the website
  String url = '';
  @HiveField(16)

  /// Notes for the password. Can be formatted with Markdown
  String notes = '';
  @HiveField(17)

  /// Custom fields created by the user. (See custom fields)
  String customFields = '';
  @HiveField(18)

  /// Security status level of the password (0 = ok, 1 = user rules violated, 2 = breached)
  int status = 0;
  @HiveField(19)

  /// Specific code for the current security status (GOOD, OUTDATED, DUPLICATE, BREACHED)
  String statusCode = '';
  @HiveField(20)

  /// SHA1 hash of the password
  String hash = '';
  @HiveField(21)

  /// UUID of the current folder of the password
  String folder = '';

  @HiveField(22)

  /// UUID of the share if the password was shared by someone else with the user
  PasswordShareModel share = PasswordShareModel();
  @HiveField(23)

  /// True if the password is shared with other users
  bool shared = false;

  @HiveField(24)

  /// Specifies if the encrypted properties can be changed. Might be false for shared passwords
  bool editable = false;

  ///list of all revisions
  @HiveField(25)
  List<PasswordModel> revisions = [];

  ///list of all tags
  @HiveField(26)
  List<String> tags = [];

  ///list of all shares
  @HiveField(27)
  List<PasswordShareModel> shares = [];

  ///favIcon as base64 String
  @HiveField(28)
  String favIconString = '';

  @override
  List<Object?> get props => super.props
    ..addAll([
      username,
      password,
      url,
      notes,
      customFields,
      status,
      statusCode,
      hash,
      folder,
      share,
      shared,
      editable,
      revision,
      tags,
      shares,
      favIconString
    ]);
  PasswordModel();
}
