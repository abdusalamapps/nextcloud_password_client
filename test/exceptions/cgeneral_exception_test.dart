// Package imports:
import 'package:flutter_test/flutter_test.dart';

// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

void main() {
  test('Credentials Exception message', () {
    GeneralException exception =
        GeneralException('some error occured', errorCode: 1412);

    expect(
        exception.toString(), "Message : some error occured, Error code 1412");

    expect(exception.toString() != "Weird message here", true);
  });
}
