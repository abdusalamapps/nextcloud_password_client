// Package imports:
import 'package:intl/intl.dart';

String formattedDate(String unixTimestamp) {
  int timestamp = int.parse(unixTimestamp);
  var date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
  return DateFormat('yyyy-MM-dd – kk:mm').format(date);
}
