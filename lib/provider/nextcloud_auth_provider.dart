// Dart imports:
// ignore_for_file: use_build_context_synchronously

// Dart imports:
import 'dart:convert';
import 'dart:typed_data';

// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter_sodium/flutter_sodium.dart';

// Package imports:
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/constants/exception_constants.dart';
import 'package:nextcloud_password_client/constants/http_constants.dart';
import 'package:nextcloud_password_client/exceptions/credentials_exception.dart';
import 'package:nextcloud_password_client/exceptions/master_password_exception.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';
import 'package:nextcloud_password_client/view_models/config_view_model.dart';
import 'package:nextcloud_password_client/view_models/credentials_view_model.dart';

class NextcloudAuthProvider {
  static NextcloudAuthProvider? _instance;

  NextcloudAuthProvider._internal();

  factory NextcloudAuthProvider() {
    _instance ??= NextcloudAuthProvider._internal();
    return _instance!;
  }

  String _serverUrl = '';
  String _username = '';
  String _password = '';
  String _masterPassword = '';
  late KeyChain _keyChain;
  late String _session;
  late BuildContext _context;
  get keyChain => _keyChain;

  Future<void> handleLogin(BuildContext context) async {
    _context = context;
    CredentialsViewModel credentialsViewModel =
        context.read<CredentialsViewModel>();
    ConfigViewModel configViewModel = context.read<ConfigViewModel>();
    _serverUrl = context.read<ConfigViewModel>().serverURL;
    _username = credentialsViewModel.userName;
    _password = credentialsViewModel.password;
    _keyChain = configViewModel.keyChain;
    _masterPassword = credentialsViewModel.masterPassword;
    _session = configViewModel.session;
    await _requestSession(_serverUrl);
  }

  Future<void> _requestSession(String serverUrl) async {
    HttpUtils().setLoginData(_serverUrl, _username, _password, _session);
    http.Response requestSessionResponse =
        await HttpUtils().httpGet(apiSessionRequest);
    final apiResponse =
        json.decode(requestSessionResponse.body) as Map<String, dynamic>;
    if (requestSessionResponse.statusCode == successStatus) {
      await _handleSessionRequestResponse(apiResponse);
    } else {
      throw CredentialsException(credentialsdError, credentialErrorCode);
    }
  }

  Future<void> _handleSessionRequestResponse(
      Map<String, dynamic> apiResponse) async {
    if (apiResponse.isEmpty) {
    } else if (apiResponse.containsKey(apiChallenge)) {
      await _handleChallenge(apiResponse[apiChallenge]);
    } else if (apiResponse.containsKey(apiToken)) {
      //TODO:handle token here
    }
  }

  Future<void> _handleChallenge(dynamic challengeValues) async {
    if (challengeValues[apiType] == apiPWDv1r1) {
      await _handlePWDV1(challengeValues[apiSalts]);
    }
  }

  Future<void> _handlePWDV1(dynamic salts) async {
    if (_masterPassword.isEmpty) {
      throw MasterPasswordException(
          masterPassworMissingdError, masterpasswordMissingErrorCode);
    } else {
      final p = utf8.encode(_masterPassword);
      final salt0 = Sodium.hex2bin(salts[0]);
      final salt1 = Sodium.hex2bin(salts[1]);
      final salt2 = Sodium.hex2bin(salts[2]);
      final x = Uint8List.fromList(p + salt0);
      final geneticHash =
          Sodium.cryptoGenerichash(Sodium.cryptoGenerichashBytesMax, x, salt1);
      final passwordHash = Sodium.cryptoPwhash(
        Sodium.cryptoBoxSeedbytes,
        geneticHash,
        salt2,
        Sodium.cryptoPwhashOpslimitInteractive,
        Sodium.cryptoPwhashMemlimitInteractive,
        Sodium.cryptoPwhashAlgDefault,
      );
      final secret = Sodium.bin2hex(passwordHash);
      http.Response challengeResponse = await HttpUtils().httpPost(
        apiSessionOpen,
        body: jsonEncode({apiChallenge: secret}),
      );
      if (challengeResponse.statusCode == 200 &&
          json.decode(challengeResponse.body)[apiSuccess]) {
        _session = _context.read<ConfigViewModel>().session =
            challengeResponse.headers['x-api-session'] as String;
        HttpUtils().setLoginData(_serverUrl, _username, _password, _session);
        _handleCSEv1r1(json.decode(challengeResponse.body)['keys']['CSEv1r1']);
        _keepSessionAlive();
      } else {
        throw MasterPasswordException(
            masterPasswordWrongError, masterpasswordWrongErrorCode);
      }
    }
  }

  void _handleCSEv1r1(String csev1r1) {
    final p = utf8.encoder.convert(_masterPassword);
    //final p = utf8.encode(_masterPassword);
    final key = csev1r1;
    final keyE = Sodium.hex2bin(key);
    final keySalt = keyE.sublist(0, Sodium.cryptoPwhashSaltbytes);
    final keyPayload = keyE.sublist(Sodium.cryptoPwhashSaltbytes);
    final decryptionKey = Sodium.cryptoPwhash(
      Sodium.cryptoBoxSeedbytes,
      p,
      keySalt,
      Sodium.cryptoPwhashOpslimitInteractive,
      Sodium.cryptoPwhashMemlimitInteractive,
      Sodium.cryptoPwhashAlgDefault,
    );
    final nonce = keyPayload.sublist(0, Sodium.cryptoSecretboxNoncebytes);
    final cipher = keyPayload.sublist(Sodium.cryptoSecretboxNoncebytes);
    final keychainObject =
        Sodium.cryptoSecretboxOpenEasy(cipher, nonce, decryptionKey);
    final keyChainJsonString = utf8.decode(keychainObject);
    _keyChain = _context.read<ConfigViewModel>().keyChain =
        KeyChain.fromMap(json.decode(keyChainJsonString));
  }

  void _keepSessionAlive() {
    Future.delayed(
      const Duration(seconds: 40),
      () async {
        await HttpUtils().httpGet(apiSessionKeepAlive);
        if (_session != '') _keepSessionAlive();
      },
    );
  }
}
