// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:nextcloud_password_client/enums/themes.dart';
import 'package:nextcloud_password_client/utils/key_chain.dart';

part 'config_model.g.dart';

@HiveType(typeId: 0)
class ConfigModel extends ChangeNotifier with EquatableMixin {
  @HiveField(0)
  String serverURL = '';
  @HiveField(1)
  bool $useLocalCopy = false;
  @HiveField(2)
  bool masterPasswordRequired = false;
  @HiveField(3)
  bool $saveCredentials = false;
  @HiveField(4)
  bool $saveMasterPassword = false;
  @HiveField(5)
  bool loginSucceeded = false;
  @HiveField(6)
  String session = '';
  @HiveField(7)
  KeyChain keyChain = KeyChain.none();
  @HiveField(8)
  Themes $selectedTheme = Themes.system;
  @HiveField(9)
  int $refreshRateInSeconds = 10;
  @HiveField(10)
  int $temporaryID = 0;

  ConfigModel();

  bool get useLocalCopy => $useLocalCopy;

  bool get saveCredentials => $saveCredentials;

  bool get saveMasterPassword => $saveMasterPassword;

  Themes get selectedTheme => $selectedTheme;

  @override
  List<Object?> get props => [
        serverURL,
        $useLocalCopy,
        masterPasswordRequired,
        $saveCredentials,
        $saveMasterPassword,
        loginSucceeded,
        session,
        $selectedTheme,
        $refreshRateInSeconds,
        $temporaryID,
      ];
}
