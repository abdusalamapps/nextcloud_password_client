// Project imports:
import 'package:nextcloud_password_client/exceptions/general_exception.dart';

class CredentialsException extends GeneralException {
  CredentialsException(super.msg, int errorCode) : super(errorCode: errorCode);

  @override
  String toString() {
    return "Message : $msg, Error code $errorCode";
  }
}
