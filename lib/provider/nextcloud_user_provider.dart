// Dart imports:
import 'dart:async';
import 'dart:convert';

// Package imports:
import 'package:http/http.dart' as http;

// Project imports:
import 'package:nextcloud_password_client/constants/api_constants.dart';
import 'package:nextcloud_password_client/utils/http_utils.dart';
import 'package:nextcloud_password_client/view_models/user_view_model.dart';

class NextcloudUserProvider {
  static Future<List<UserViewModel>> retrieveUsers() async {
    http.Response requestSessionResponse = await HttpUtils().httpGet(apiGetUsers);
   var body = json.decode(requestSessionResponse.body);    
    List<UserViewModel> users = [];
    for ( var user in body.entries ) {
      users.add(UserViewModel.fromMap(user.key, user.value));
    }
    return users;
  }
 
  static void shareWithuser(String password, String user) async{
   http.Response shareWithUserResponse = await HttpUtils().httpPost(apiShareWithUser,body: 
      jsonEncode({'password':password, 'receiver':user}));
   if (shareWithUserResponse.statusCode == 200) {
   
   }
  }
}
