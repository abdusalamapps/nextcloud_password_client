// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:pluto_grid/pluto_grid.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/view_models/password_list_view_model.dart';
import 'package:nextcloud_password_client/view_models/password_view_model.dart';
import 'package:nextcloud_password_client/widgets/password_grid/password_cell.dart';

List<PlutoRow> getPasswordRows(BuildContext context,
    [List<PasswordViewModel>? models]) {
  List<PlutoRow> gridRows = [];
  if (models == null || models.isEmpty) {
    models = context.read<PasswordListViewModel>().filteredPasswordViewModels;
  }
  for (PasswordViewModel password in models) {
    gridRows.add(PlutoRow(
        key: ValueKey<String>(password.id),
        cells: getPasswordCells(context, password)));
  }
  return gridRows;
}
