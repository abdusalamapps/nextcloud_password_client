// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

// Project imports:
import 'package:nextcloud_password_client/constants/settings_list_items.dart';
import 'package:nextcloud_password_client/utils/view_state.dart';

Widget settingsTopicList(BuildContext context) {
  List<String> settingsItemsList = [
    AppLocalizations.of(context)!.login,
    AppLocalizations.of(context)!.systemMenu,
    AppLocalizations.of(context)!.themes,
    AppLocalizations.of(context)!.about
  ];
  return ListView.builder(
    itemCount: settingsItemsList.length,
    itemBuilder: (BuildContext context, int index) {
      return ListTile(
        onTap: () => context.read<ViewState>().selectedSettingsItem =
            SettingsItems.values[index],
        title: Text(
          settingsItemsList[index],
        ),
      );
    },
  );
}
