class GeneralException implements Exception {
  String msg = '';
  int? errorCode = 0;

  GeneralException(this.msg, {this.errorCode});

  @override
  String toString() {
    return "Message : $msg, Error code $errorCode";
  }
}
