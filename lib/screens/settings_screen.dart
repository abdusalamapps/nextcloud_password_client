// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:nextcloud_password_client/screens/window_border.dart';
import 'package:nextcloud_password_client/widgets/settings/settings.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  State<StatefulWidget> createState() => SettingsScreenState();
}

class SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: getWindowBorder(context, true), body: getSettings(context));
  }
}
